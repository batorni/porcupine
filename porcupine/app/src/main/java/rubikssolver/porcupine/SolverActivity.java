package rubikssolver.porcupine;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;


public class SolverActivity extends AppCompatActivity {

    int cubeColors[];
    int faces[][][];

    public Bitmap [] images;

    String colors[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solver);

        //Will be populated later in the analyze colors method
        cubeColors = new int[6];
        resetColors();

        faces = new int[6][3][3];

        // Used for Debugging only
        colors = new String[6];
        colors[0] = "W";
        colors[1] = "Y";
        colors[2] = "O";
        colors[3] = "R";
        colors[4] = "B";
        colors[5] = "G";

		//[0] = White
		//[1] = Yellow
		//[2] = Orange
		//[3] = Red
		//[4] = Blue
		//[5] = Green
		// ONLY ADD RESPECTIVE COLOR TO RESPECTIVE INDEX
        images = new Bitmap[6];
//
//        ImageView imageView = (ImageView) findViewById(R.id.imageView);
//        ImageView imageView2 = (ImageView) findViewById(R.id.imageView2);
//        ImageView imageView3 = (ImageView) findViewById(R.id.imageView3);
//        ImageView imageView4 = (ImageView) findViewById(R.id.imageView4);
//        ImageView imageView5 = (ImageView) findViewById(R.id.imageView6);
//        ImageView imageView6 = (ImageView) findViewById(R.id.imageView5);
//        imageView.setBackgroundColor(Color.BLACK);

    }

    private void resetColors() {
        cubeColors[0] = Color.WHITE;
        cubeColors[1] = Color.YELLOW;
        cubeColors[2] = -26368;
        cubeColors[3] = Color.RED;
        cubeColors[4] = Color.BLUE;
        cubeColors[5] = Color.GREEN;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rubikscube, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_settings: {
                System.out.println("action_settings was selected");
                return true;
            }
            case R.id.solver: {
                System.out.println("solver was selected");
                Intent intent = new Intent(this, SolverActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.timer: {
                System.out.println("timer was selected");
                Intent intent = new Intent(this, TimerActivity.class);
                startActivity(intent);
                return true;
            }


        }

        return super.onOptionsItemSelected(item);
    }



    public int findColor(int unkCol) {

        int rgbDist = 99999999;
        int returnColor = 0;

        for(int i = 0; i < 6; i++) {
            int test =  Math.abs(Color.red(unkCol) - Color.red(cubeColors[i])) +
                        Math.abs(Color.green(unkCol) - Color.green(cubeColors[i])) +
                        Math.abs(Color.blue((unkCol) - Color.blue(cubeColors[i])));

            if(rgbDist > test) {
                rgbDist = test;
                returnColor = i;
            }
        }

        return returnColor + 1;
    }

    public void analyzeColors() {
        
        /*
         * 1 = White
         * 2 = Yellow
         * 3 = Orange
         * 4 = Red
         * 5 = Blue
         * 6 = Green
         */

        faces[0][1][1] = 1; // White
        faces[1][1][1] = 2; // Yellow
        faces[2][1][1] = 3; // Orange
        faces[3][1][1] = 4; // Red
        faces[4][1][1] = 5; // Blue
        faces[5][1][1] = 6; // Green

        int edgeSize = (images[0].getHeight() / 3);

        // This nested loop is used to find average center color
        // of each side.

        int runs = 0;
        int avgR = 0;
        int avgG = 0;
        int avgB = 0;
        resetColors();

        for (int i = 0; i < images.length; i++) {
            avgR = 0;
            avgG = 0;
            avgB = 0;
            runs = 0;

            for (int x = (edgeSize + 5); x <= ((edgeSize * 2) - 5); x++) {
                for (int y = (edgeSize + 5); y <= ((edgeSize * 2) - 5); y++) {
                    //System.out.println("NEVER GONNA GIVE YOU UP, NEVER GONNA LET YOU DOWN " + i + " " + y + " " + x);
                    int r = Color.red(images[i].getPixel(x, y));
                    int g = Color.green(images[i].getPixel(x, y));
                    int b = Color.blue(images[i].getPixel(x, y));

                    avgR += r;
                    avgG += g;
                    avgB += b;

                    runs++;

                }
            }

            avgR = (avgR / runs);
            avgG = (avgG / runs);
            avgB = (avgB / runs);

            cubeColors[i] = (cubeColors[i] + Color.rgb(avgR, avgG, avgB)) / 2;

        }

        for (int i = 0; i < cubeColors.length; i++) {
            System.out.println(colors[i] + ": " + cubeColors[i]);
        }

        // Now it's time to find colors of all cubes.
        for (int side = 0; side < 6; side++) { //for each side
            for (int i = 0; i < 3; i++) { //for each row
                for (int j = 0; j < 3; j++) { //for each column
                    avgR = 0;
                    avgG = 0;
                    avgB = 0;
                    runs = 0;

                    if (i == 1 && j == 1) {
                        continue;
                    }

                    for (int x = (edgeSize * i) + 5; x <= ((edgeSize * (i + 1)) - 5); x++) { //for each x pixel

                        for (int y = (edgeSize * j) + 5; y <= ((edgeSize * (j + 1)) - 5); y++) { //for each y pixel

                            int r = Color.red(images[side].getPixel(x, y));
                            int g = Color.green(images[side].getPixel(x, y));
                            int b = Color.blue(images[side].getPixel(x, y));

                            avgR += r;
                            avgG += g;
                            avgB += b;

                            runs++;
                        }
                    }

                    avgR = (avgR / runs);
                    avgG = (avgG / runs);
                    avgB = (avgB / runs);

                    int newColor = findColor(Color.rgb(avgR, avgG, avgB));
                    faces[side][i][j] = newColor;
                }
            }
        }

        for (int num = 0; num < faces.length; num++) {
            System.out.println(colors[num] + " side:");
            for (int x = 0; x < 3; x++) {
                for (int y = 0; y < 3; y++) {
                    System.out.print(colors[(faces[num][x][y]) - 1] + " ");
                }
                System.out.println("");
            }
        }
        System.out.println("----------------");
    }

        //THIS ORDER IS EXCLUSIVELY USED FOR THE CONSTRUCTOR
        //0 = orange
        //1 = red
        //2 = yellow
        //3 = green
        //4 = blue
        //5 = white
        
        // Cube cube = new Cube(faces[2], faces[3], faces[1], faces[5], faces[4], faces[0]);

        //Frank's awesome Method here
        public Bitmap nineColorsPerSide()
        {
            for(int num = 0; num < faces.length; num++) {
                System.out.println(colors[num] + " side:");
                for(int x = 0; x < 3; x ++) {
                    for(int y = 0; y < 3; y++) {
                        System.out.print(colors[(faces[num][x][y]) - 1] + " ");
                        switch(colors[(faces[num][x][y]) - 1]){
                            case "W":
//                                ImageView imageView = (ImageView) findViewById(R.id.imageView);
//                                imageView.setBackgroundColor(Color.BLACK);
                                break;
                            case "Y":
                                break;
                            case "O":
                                break;
                            case "R":
                                break;
                            case "B":
                                break;
                            case "G":
                                break;
                        }
                    }
                    System.out.println("");
                    System.out.println("");
                    System.out.println("");
                }
        }
            return null;
    }


    private Bitmap loadImage(String path)
    {

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            Bitmap b = BitmapFactory.decodeFile(path, options);

            return b;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public void onWhiteClick(View view) {

        if(faces[0][0][0] == 0) {
            startActivityForResult(new Intent(getApplicationContext(), White.class), 0);
            System.out.println("You hit the White button!");
        } else {
            Storage.getInstance().setCurrentFace(faces[0], 0);
            startActivity(new Intent(getApplicationContext(), cubeModifier.class));
        }

    }
    public void onGreenClick(View view) {
        if(faces[5][0][0] == 0) {
            startActivityForResult(new Intent(getApplicationContext(), Green.class), 5);
            System.out.println("You hit the Green button!");
        } else {
            Storage.getInstance().setCurrentFace(faces[5], 5);
            startActivity(new Intent(getApplicationContext(), cubeModifier.class));
        }
    }
    public void onYellowClick(View view) {
        if(faces[1][0][0] == 0) {
            startActivityForResult(new Intent(getApplicationContext(), Yellow.class), 1);
            System.out.println("You hit the Yellow button!");
        } else {
            Storage.getInstance().setCurrentFace(faces[1], 1);
            startActivity(new Intent(getApplicationContext(), cubeModifier.class));
        }
    }
    public void onBlueClick(View view) {
        if(faces[4][0][0] == 0) {
            startActivityForResult(new Intent(getApplicationContext(), Blue.class), 4);
            System.out.println("You hit the Blue button!");
        } else {
            Storage.getInstance().setCurrentFace(faces[4], 4);
            startActivity(new Intent(getApplicationContext(), cubeModifier.class));
        }
    }
    public void onOrangeClick(View view) {
        if(faces[2][0][0] == 0) {
            startActivityForResult(new Intent(getApplicationContext(), Orange.class), 2);
            System.out.println("You hit the Orange button!");
        }
        else {
            Storage.getInstance().setCurrentFace(faces[2], 2);
            startActivity(new Intent(getApplicationContext(), cubeModifier.class));
        }
    }
    public void onRedClick(View view) {
        if(faces[3][0][0] == 0) {
            startActivityForResult(new Intent(getApplicationContext(), Red.class), 3);
            System.out.println("You hit the Red button!");
        } else {
            Storage.getInstance().setCurrentFace(faces[3], 3);
            startActivity(new Intent(getApplicationContext(), cubeModifier.class));
        }
    }

    public void onSolveClick(View view) {

        Button b = (Button) view;

        if(b.getText().equals("Analyze")) {

            analyzeColors();

            for (int num = 0; num < faces.length; num++) {
                System.out.println(colors[num] + " side:");
                for (int x = 0; x < 3; x++) {
                    for (int y = 0; y < 3; y++) {
                        System.out.print(colors[(faces[num][x][y]) - 1] + " ");
                    }
                    System.out.println("");
                }
            }
            System.out.println("----------------");

            b.setText("Solve");

        }

        else if(b.getText().equals("Solve")) {

            Cube cube = new Cube(faces[2], faces[3], faces[1], faces[5], faces[4], faces[0]);
            Storage.getInstance().setCube(faces);

            ArrayList<String> solution  = Solve.solveCube(cube);

            Storage.getInstance().setSolution(solution);

            startActivity(new Intent(getApplicationContext(), ShowList.class));

            for (String str : cube.getResult()) {
                System.out.println(str);
            }
        }
    }

    public void onActivityResult(int resultCode, int requestCode, Intent data){
        int arrayCount = 0;
        File directory = new File(Environment.getExternalStorageDirectory()+File.separator+"porcupine_images");
        if(!directory.exists()){
            return;
        }
        for(int i = 0; i < 6; i++){
            switch(i) {
                case 0:
                    images[0] = loadImage(Environment.getExternalStorageDirectory()+File.separator+"porcupine_images"+File.separator+"white.jpg");
                    System.out.println("loading white");
                    if(images[0] == null){
                        return;
                    }
                    arrayCount++;
                    break;
                case 1:
                    images[1] = loadImage(Environment.getExternalStorageDirectory()+File.separator+"porcupine_images"+File.separator+"yellow.jpg");
                    System.out.println("loading yellow");
                    if(images[1] == null){
                        return;
                    }
                    arrayCount++;
                    break;
                case 2:
                    images[2] = loadImage(Environment.getExternalStorageDirectory()+File.separator+"porcupine_images"+File.separator+"orange.jpg");
                    System.out.println("loading orange");
                    if(images[2] == null){
                        return;
                    }
                    arrayCount++;
                    break;
                case 3:
                    images[3] = loadImage(Environment.getExternalStorageDirectory()+File.separator+"porcupine_images"+File.separator+"red.jpg");
                    System.out.println("loading red");
                    if(images[3] == null){
                        return;
                    }
                    arrayCount++;
                    break;
                case 4:
                    images[4] = loadImage(Environment.getExternalStorageDirectory()+File.separator+"porcupine_images"+File.separator+"blue.jpg");
                    System.out.println("loading blue");
                    if(images[4] == null){
                        return;
                    }
                    arrayCount++;
                    break;
                case 5:
                    images[5] = loadImage(Environment.getExternalStorageDirectory()+File.separator+"porcupine_images"+File.separator+"green.jpg");
                    System.out.println("loading green");
                    if(images[5] == null){
                        return;
                    }
                    arrayCount++;
                    break;
            }
        }
        if(arrayCount == 6) {
            Button b = (Button)findViewById(R.id.solveButton);

            b.setEnabled(true);
        }

    }
}



