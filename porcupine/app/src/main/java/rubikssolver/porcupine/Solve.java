package rubikssolver.porcupine;
import java.util.ArrayList;

/**
 * This is the solve class. This is all of the helper methods that are used to
 * then solve the cube. Some of the helper methods have their own helper methods
 * to help and make the code less redundant. Lastly, there is a method in here
 * which returns the solved arraylist.
 */

public class Solve {

    /**
     * The method solveCross solves the cross of the white face. It generally
     * takes less than 15 moves to solve. The comments below each if statement
     * says what side of the white cross was solved
     *
     * @param cube
     */

    static public void solveCross(Cube cube) {
        int count = 0;

        // makes sure that it does not loop too many times
        while (1 == 1) {
            if (count > 200) {
                break;
            }
            // if the cross is solved, it will break out of loop and complete
            if (cube.getBottomFace()[0][1] == 1 && cube.getFrontFace()[2][1] == 3 && cube.getBottomFace()[1][0] == 1
                    && cube.getLeftFace()[2][1] == 6 && cube.getBottomFace()[1][2] == 1
                    && cube.getRightFace()[2][1] == 5 && cube.getBottomFace()[2][1] == 1
                    && cube.getBackFace()[2][1] == 4) {
                break;
            }

            // This will then

            if (cube.getFrontFace()[1][2] == 1) {
                // done
                if (cube.getRightFace()[1][0] == 5) {
                    cube.turnRightPrime();
                    // Blue
                } else if (cube.getRightFace()[1][0] == 3) {
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    // Orange
                } else if (cube.getRightFace()[1][0] == 4) {
                    cube.turnDownPrime();
                    cube.turnRightPrime();
                    cube.turnDown();
                    // Red
                } else if (cube.getRightFace()[1][0] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDown();
                    cube.turnDown();
                    // Green
                }

            }

            if (cube.getRightFace()[1][0] == 1) {
                // done
                if (cube.getFrontFace()[1][2] == 5) {
                    cube.turnDownPrime();
                    cube.turnFront();
                    cube.turnDown();
                    // Blue
                } else if (cube.getFrontFace()[1][2] == 3) {
                    cube.turnFront();
                    // Orange
                } else if (cube.getFrontFace()[1][2] == 4) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnFront();
                    cube.turnDown();
                    cube.turnDown();
                    // Red
                } else if (cube.getFrontFace()[1][2] == 6) {
                    cube.turnDown();
                    cube.turnFront();
                    cube.turnDownPrime();
                    // Green
                }

            }

            if (cube.getFrontFace()[1][0] == 1) {
                // done
                if (cube.getLeftFace()[1][2] == 5) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnLeft();
                    cube.turnDown();
                    cube.turnDown();
                    // Blue
                } else if (cube.getLeftFace()[1][2] == 3) {
                    cube.turnDownPrime();
                    cube.turnLeft();
                    cube.turnDown();
                    // Orange
                } else if (cube.getLeftFace()[1][2] == 4) {
                    cube.turnDown();
                    cube.turnLeft();
                    cube.turnDownPrime();
                    // Red
                } else if (cube.getLeftFace()[1][2] == 6) {
                    cube.turnLeft();
                    // Green
                }

            }

            if (cube.getLeftFace()[1][2] == 1) {
                // done
                if (cube.getFrontFace()[1][0] == 5) {
                    cube.turnDownPrime();
                    cube.turnFrontPrime();
                    cube.turnDown();
                    // Blue
                } else if (cube.getFrontFace()[1][0] == 3) {
                    cube.turnFrontPrime();
                    // Orange
                } else if (cube.getFrontFace()[1][0] == 4) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnFrontPrime();
                    cube.turnDown();
                    cube.turnDown();
                    // Red
                } else if (cube.getFrontFace()[1][0] == 6) {
                    cube.turnDown();
                    cube.turnFrontPrime();
                    cube.turnDownPrime();
                    // Green
                }

            }

            if (cube.getBackFace()[1][2] == 1) {
                // done
                if (cube.getRightFace()[1][2] == 5) {
                    cube.turnRight();
                    // Blue
                } else if (cube.getRightFace()[1][2] == 3) {
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnDownPrime();
                    // Orange
                } else if (cube.getRightFace()[1][2] == 4) {
                    cube.turnDownPrime();
                    cube.turnRight();
                    cube.turnDown();
                    // Red =
                } else if (cube.getRightFace()[1][2] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnDown();
                    cube.turnDown();
                    // Green
                }

            }

            if (cube.getRightFace()[1][2] == 1) {
                // done
                if (cube.getBackFace()[1][2] == 5) {
                    cube.turnDown();
                    cube.turnBackPrime();
                    cube.turnDownPrime();
                    // Blue
                } else if (cube.getBackFace()[1][2] == 3) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnBackPrime();
                    cube.turnDown();
                    cube.turnDown();
                    // Orange
                } else if (cube.getBackFace()[1][2] == 4) {
                    cube.turnBackPrime();
                    // Red
                } else if (cube.getBackFace()[1][2] == 6) {
                    cube.turnDownPrime();
                    cube.turnBackPrime();
                    cube.turnDown();
                    // Green
                }

            }

            if (cube.getBackFace()[1][0] == 1) {

                if (cube.getLeftFace()[1][0] == 5) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnLeftPrime();
                    cube.turnDown();
                    cube.turnDown();
                    // Blue
                } else if (cube.getLeftFace()[1][0] == 3) {
                    cube.turnDownPrime();
                    cube.turnLeftPrime();
                    cube.turnDown();
                    // Orange
                } else if (cube.getLeftFace()[1][0] == 4) {
                    cube.turnDown();
                    cube.turnLeftPrime();
                    cube.turnDownPrime();
                    // Red
                } else if (cube.getLeftFace()[1][0] == 6) {
                    cube.turnLeftPrime();
                    // Green
                }

            }

            if (cube.getLeftFace()[1][0] == 1) {
                // done
                if (cube.getBackFace()[1][0] == 5) {
                    cube.turnDown();
                    cube.turnBack();
                    cube.turnDownPrime();
                    // Blue
                } else if (cube.getBackFace()[1][0] == 3) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnBack();
                    cube.turnDown();
                    cube.turnDown();
                    // Orange
                } else if (cube.getBackFace()[1][0] == 4) {
                    cube.turnBack();
                    // Red
                } else if (cube.getBackFace()[1][0] == 6) {
                    cube.turnDownPrime();
                    cube.turnBack();
                    cube.turnDown();
                    // Green
                }

            }

            if (cube.getTopFace()[1][2] == 1) {
                // done
                if (cube.getRightFace()[0][1] == 5) {
                    cube.turnRight();
                    cube.turnRight();
                    // Blue
                } else if (cube.getRightFace()[0][1] == 3) {
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnRight();
                    cube.turnDownPrime();
                    // Orange
                } else if (cube.getRightFace()[0][1] == 4) {
                    cube.turnDownPrime();
                    cube.turnRight();
                    cube.turnRight();
                    cube.turnDown();
                    // Red
                } else if (cube.getRightFace()[0][1] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnRight();
                    cube.turnDown();
                    cube.turnDown();
                    // Green
                }

            }

            if (cube.getFrontFace()[0][1] == 1) {
                cube.turnUpPrime();
            } else if (cube.getLeftFace()[0][1] == 1) {
                cube.turnUp();
                cube.turnUp();
            } else if (cube.getBackFace()[2][1] == 1) {
                cube.turnUpPrime();
            }

            if (cube.getRightFace()[0][1] == 1) {

                if (cube.getTopFace()[1][2] == 5) {
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    cube.turnFront();
                    cube.turnDown();
                    // Blue
                } else if (cube.getTopFace()[1][2] == 3) {
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    cube.turnFront();
                    // Orange
                } else if (cube.getTopFace()[1][2] == 4) {
                    cube.turnDownPrime();
                    cube.turnRight();
                    cube.turnDown();
                    cube.turnBackPrime();
                    // Red
                } else if (cube.getTopFace()[1][2] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    cube.turnFront();
                    cube.turnDownPrime();
                    // Green
                }

            }

            if (cube.getTopFace()[2][1] == 1) {
                // done
                if (cube.getFrontFace()[0][1] == 5) {
                    cube.turnUpPrime();
                    cube.turnRight();
                    cube.turnRight();
                    // Blue
                } else if (cube.getFrontFace()[0][1] == 3) {
                    cube.turnFront();
                    cube.turnFront();
                    // Orange
                } else if (cube.getFrontFace()[0][1] == 4) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnBack();
                    cube.turnBack();
                    // Red
                } else if (cube.getFrontFace()[0][1] == 6) {
                    cube.turnUp();
                    cube.turnLeft();
                    cube.turnLeft();
                    // Green
                }

            }

            if (cube.getTopFace()[1][0] == 1) {
                // done
                if (cube.getLeftFace()[0][1] == 5) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnRight();
                    cube.turnRight();
                    // Blue
                } else if (cube.getLeftFace()[0][1] == 3) {
                    cube.turnUpPrime();
                    cube.turnFront();
                    cube.turnFront();
                    // Orange
                } else if (cube.getLeftFace()[0][1] == 4) {
                    cube.turnUp();
                    cube.turnBack();
                    cube.turnBack();
                    // Red
                } else if (cube.getLeftFace()[0][1] == 6) {
                    cube.turnLeft();
                    cube.turnLeft();
                    // Green
                }

            }

            if (cube.getTopFace()[0][1] == 1) {

                if (cube.getBackFace()[2][1] == 5) {
                    cube.turnUp();
                    cube.turnRight();
                    cube.turnRight();
                    // Blue
                } else if (cube.getBackFace()[2][1] == 3) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnFront();
                    cube.turnFront();
                    // Orange
                } else if (cube.getBackFace()[2][1] == 4) {

                    cube.turnBack();
                    cube.turnBack();
                    // Red
                } else if (cube.getBackFace()[2][1] == 6) {
                    cube.turnUpPrime();
                    cube.turnLeft();
                    cube.turnLeft();
                    // Green
                }

            }

            if (cube.getTopFace()[1][2] == 1) {

                if (cube.getRightFace()[0][1] == 5) {
                    cube.turnRight();
                    cube.turnRight();
                    // Blue
                } else if (cube.getRightFace()[0][1] == 3) {
                    cube.turnUp();
                    cube.turnFront();
                    cube.turnFront();
                    // Orange
                } else if (cube.getRightFace()[0][1] == 4) {
                    cube.turnUpPrime();
                    cube.turnBack();
                    cube.turnBack();
                    // Red
                } else if (cube.getRightFace()[0][1] == 6) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnLeft();
                    cube.turnLeft();
                    // Green
                }

            }
            // this is for cases when the white piece is flipped
            if (cube.getFrontFace()[2][1] == 1) {
                cube.turnFront();
            }
            if (cube.getLeftFace()[2][1] == 1) {
                cube.turnLeft();
            }
            if (cube.getRightFace()[2][1] == 1) {
                cube.turnRight();
            }
            if (cube.getBackFace()[0][1] == 1) {
                cube.turnBack();
            }

			/*
			 * This case checks if a white piece is on the right side, but not
			 * in the right position. This is checked last because it will most
			 * likely get kicked out of its position by the time it reaches
			 * here.
			 */
            if (cube.getBottomFace()[0][1] == 1 && cube.getFrontFace()[2][1] != 3) {
                cube.turnFront();
            }
            if (cube.getBottomFace()[1][0] == 1 && cube.getLeftFace()[2][1] != 6) {
                cube.turnLeft();
            }
            if (cube.getBottomFace()[1][2] == 1 && cube.getRightFace()[2][1] != 5) {
                cube.turnRight();
            }
            if (cube.getBottomFace()[2][1] == 1 && cube.getBackFace()[0][1] != 4) {

                cube.turnBack();
            }
            count++; // increment count

        }

    }

    /**
     * The method solveFirstLayer is the next step in solving the cube. This
     * method solves the white face of the cube, and makes sure that all of the
     * corner pieces of the white cube are aligned correctly. The comments below
     * each of the if statements describe which one of the corners that it just
     * solved.
     *
     * @param cube
     */

    static public void solveFirstLayer(Cube cube) {

        int count = 0; // makes a new count

        while (1 == 1) {
            if (count > 500) {
                break;
            }
            // if the white face is solved, it will break
            if (cube.getBottomFace()[0][0] == 1 && cube.getFrontFace()[2][0] == 3 && cube.getBottomFace()[0][2] == 1
                    && cube.getFrontFace()[2][2] == 3 && cube.getBottomFace()[2][0] == 1
                    && cube.getBackFace()[0][0] == 4 && cube.getBottomFace()[2][2] == 1
                    && cube.getBackFace()[0][2] == 4) {
                break;
            }

            // White is on the front face
            if (cube.getRightFace()[0][0] == 1) {
                cube.turnUp();
            } else if (cube.getBackFace()[2][2] == 1) {
                cube.turnUp();
                cube.turnUp();
            } else if (cube.getLeftFace()[0][0] == 1) {
                cube.turnUpPrime();
            }

            if (cube.getFrontFace()[0][0] == 1) {
                if (cube.getTopFace()[2][0] == 6) {
                    cube.turnUp();
                    cube.turnLeft();
                    cube.turnUp();
                    cube.turnLeftPrime();
                    // redgreen
                } else if (cube.getTopFace()[2][0] == 5) {
                    cube.turnUpPrime();
                    cube.turnRight();
                    cube.turnUp();
                    cube.turnRightPrime();
                    // blueorange
                } else if (cube.getTopFace()[2][0] == 3) {
                    cube.turnFront();
                    cube.turnUp();
                    cube.turnFrontPrime();
                    // greenorange
                } else if (cube.getTopFace()[2][0] == 4) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnBack();
                    cube.turnUp();
                    cube.turnBackPrime();
                    // bluered
                }
            }

            // WHITE IS ON THE LEFT FACE
            if (cube.getRightFace()[0][2] == 1) {
                cube.turnUp();
                cube.turnUp();
            } else if (cube.getBackFace()[2][0] == 1) {
                cube.turnUpPrime();
            } else if (cube.getFrontFace()[0][2] == 1) {
                cube.turnUp();
            }

            if (cube.getLeftFace()[0][2] == 1) {

                if (cube.getTopFace()[2][0] == 6) {
                    cube.turnLeftPrime();
                    cube.turnUpPrime();
                    cube.turnLeft();
                    // greenorange

                } else if (cube.getTopFace()[2][0] == 5) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnRightPrime();
                    cube.turnUpPrime();
                    cube.turnRight();
                    // bluered
                } else if (cube.getTopFace()[2][0] == 3) {
                    cube.turnUpPrime();
                    cube.turnFrontPrime();
                    cube.turnUpPrime();
                    cube.turnFront();
                    // blueorange
                } else if (cube.getTopFace()[2][0] == 4) {
                    cube.turnUp();
                    cube.turnBackPrime();
                    cube.turnUpPrime();
                    cube.turnBack();
                    // greenred
                }

				/*
				 * These next four if statements are in the case that a white
				 * piece is in a spot where we don't want it, and kicks it out
				 */
                if (cube.getBottomFace()[0][0] == 1 && cube.getFrontFace()[2][0] != 3) {
                    cube.turnLeftPrime();
                    cube.turnUpPrime();
                    cube.turnLeft();
                }
                if (cube.getBottomFace()[0][2] == 1 && cube.getFrontFace()[2][2] != 3) {
                    cube.turnRight();
                    cube.turnUp();
                    cube.turnRightPrime();
                }
                if (cube.getBottomFace()[2][0] == 1 && cube.getBackFace()[0][0] != 4) {
                    cube.turnBackPrime();
                    cube.turnUpPrime();
                    cube.turnBack();
                }
                if (cube.getBottomFace()[2][2] == 1 && cube.getBackFace()[0][2] != 4) {
                    cube.turnBack();
                    cube.turnUp();
                    cube.turnBackPrime();
                }

            }

            // blueorange meet
            if (cube.getLeftFace()[2][0] == 1) {
                cube.turnLeft();
                cube.turnUp();
                cube.turnLeftPrime();
            } else if (cube.getFrontFace()[2][2] == 1) {
                cube.turnRight();
                cube.turnUpPrime();
                cube.turnRightPrime();
                // greenorange meet
            } else if (cube.getFrontFace()[2][0] == 1) {
                cube.turnLeftPrime();
                cube.turnUp();
                cube.turnLeft();
            } else if (cube.getLeftFace()[2][2] == 1) {
                cube.turnLeftPrime();
                cube.turnUpPrime();
                cube.turnLeft();
                // blueredmeet
            } else if (cube.getBackFace()[0][2] == 1) {
                cube.turnBack();
                cube.turnUp();
                cube.turnBackPrime();
            } else if (cube.getRightFace()[2][2] == 1) {
                cube.turnBack();
                cube.turnUpPrime();
                cube.turnBackPrime();
                // greenred meet
            } else if (cube.getBackFace()[0][0] == 1) {
                cube.turnBackPrime();
                cube.turnUpPrime();
                cube.turnBack();
            } else if (cube.getFrontFace()[2][2] == 1) {
                cube.turnBackPrime();
                cube.turnUp();
                cube.turnBack();
            } else if (cube.getRightFace()[2][0] == 1) {
                cube.turnRight();
                cube.turnUp();
                cube.turnRightPrime();
            }

			/*
			 * These if statements occur in the case that there is a white piece
			 * on the top face. It then brings the white piece onto the front
			 * face, and continues the while loop.
			 *
			 */

            if (cube.getTopFace()[2][2] == 1) {
                if (cube.getBottomFace()[0][2] != 1) {
                    cube.turnRight();
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnRightPrime();
                } else
                    cube.turnUp();
            }
            if (cube.getTopFace()[2][0] == 1) {
                if (cube.getBottomFace()[0][0] != 1) {
                    cube.turnLeftPrime();
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnLeft();
                } else
                    cube.turnUp();
            }
            if (cube.getTopFace()[0][0] == 1) {
                if (cube.getBottomFace()[2][0] != 1) {
                    cube.turnBackPrime();
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnBack();
                } else
                    cube.turnUp();
            }
            if (cube.getTopFace()[0][2] == 1) {
                if (cube.getBottomFace()[2][2] != 1) {
                    cube.turnBack();
                    cube.turnUpPrime();
                    cube.turnBackPrime();
                } else
                    cube.turnUp();
            }
            if (count == 50) {
                if (cube.getFrontFace()[2][0] != 3) {
                    cube.turnLeftPrime();
                    cube.turnUpPrime();
                    cube.turnLeft();
                } else if (cube.getFrontFace()[2][2] != 3) {
                    cube.turnRight();
                    cube.turnUp();
                    cube.turnRightPrime();
                } else if (cube.getRightFace()[2][2] != 5) {
                    cube.turnBack();
                    cube.turnUp();
                    cube.turnBackPrime();
                }
            }

            count++; // increment count

        }

    }

	/*
	 * These next 8 methods are helper methods for the second layer solver.
	 * Rather than have to call the 8 moves over and over again, this helps to
	 * reduce redundancy in the code. The word before solve describes where the
	 * desired piece is to move to, and the word after solve describes which
	 * face that the algorithm will be applied to. The first two do not have
	 * words after solve as they are the front face
	 */

    public static void rightSolve(Cube cube) {
        cube.turnUp();
        cube.turnRight();
        cube.turnUpPrime();
        cube.turnRightPrime();
        cube.turnUpPrime();
        cube.turnFrontPrime();
        cube.turnUp();
        cube.turnFront();
    }

    public static void leftSolve(Cube cube) {
        cube.turnUpPrime();
        cube.turnLeftPrime();
        cube.turnUp();
        cube.turnLeft();
        cube.turnUp();
        cube.turnFront();
        cube.turnUpPrime();
        cube.turnFrontPrime();
    }

    public static void rightSolveLeft(Cube cube) {
        cube.turnUp();
        cube.turnFront();
        cube.turnUpPrime();
        cube.turnFrontPrime();
        cube.turnUpPrime();
        cube.turnLeftPrime();
        cube.turnUp();
        cube.turnLeft();
    }

    public static void leftSolveLeft(Cube cube) {
        cube.turnUpPrime();
        cube.turnBackPrime();
        cube.turnUp();
        cube.turnBack();
        cube.turnUp();
        cube.turnLeft();
        cube.turnUpPrime();
        cube.turnLeftPrime();
    }

    public static void rightSolveRight(Cube cube) {
        cube.turnUp();
        cube.turnBack();
        cube.turnUpPrime();
        cube.turnBackPrime();
        cube.turnUpPrime();
        cube.turnRightPrime();
        cube.turnUp();
        cube.turnRight();

    }

    public static void leftSolveRight(Cube cube) {
        cube.turnUpPrime();
        cube.turnFrontPrime();
        cube.turnUp();
        cube.turnFront();
        cube.turnUp();
        cube.turnRight();
        cube.turnUpPrime();
        cube.turnRightPrime();
    }

    public static void rightSolveBack(Cube cube) {
        cube.turnUp();
        cube.turnLeft();
        cube.turnUpPrime();
        cube.turnLeftPrime();
        cube.turnUpPrime();
        cube.turnBackPrime();
        cube.turnUp();
        cube.turnBack();
    }

    public static void leftSolveBack(Cube cube) {
        cube.turnUpPrime();
        cube.turnRightPrime();
        cube.turnUp();
        cube.turnRight();
        cube.turnUp();
        cube.turnBack();
        cube.turnUpPrime();
        cube.turnBackPrime();
    }

    /**
     * This method solves the second layer of the cube. It uses the 8 helper
     * methods to help complete this task. The color that it is attempting to
     * solve is listed in a comment above each of the set of 4 if statements.
     *
     * @param cube
     */

    static public void solveSecondLayer(Cube cube) {
        int count = 0;

        while (1 == 1) {
            count++;
            if (count > 500) {
                break;
            }
            // if the second layer is solved
            if (cube.getFrontFace()[1][0] == 3 && cube.getFrontFace()[1][2] == 3 && cube.getLeftFace()[1][0] == 6
                    && cube.getLeftFace()[1][2] == 6 && cube.getRightFace()[1][0] == 5 && cube.getRightFace()[1][2] == 5
                    && cube.getBackFace()[1][0] == 4 && cube.getBackFace()[1][2] == 4) {

                break;
            }

            // orangegreen
            if (cube.getFrontFace()[0][1] == 3 && cube.getTopFace()[2][1] == 5) {
                rightSolve(cube);
            }
            if (cube.getLeftFace()[0][1] == 3 && cube.getTopFace()[1][0] == 5) {
                cube.turnUpPrime();
                rightSolve(cube);
            }
            if (cube.getRightFace()[0][1] == 3 && cube.getTopFace()[1][2] == 5) {
                cube.turnUp();
                rightSolve(cube);
            }
            if (cube.getBackFace()[2][1] == 3 && cube.getTopFace()[0][1] == 5) {
                cube.turnUp();
                cube.turnUp();
                rightSolve(cube);
            }

            // orangeblue
            if (cube.getFrontFace()[0][1] == 3 && cube.getTopFace()[2][1] == 6) {
                leftSolve(cube);
            }
            if (cube.getLeftFace()[0][1] == 3 && cube.getTopFace()[1][0] == 6) {
                cube.turnUpPrime();
                leftSolve(cube);
            }
            if (cube.getRightFace()[0][1] == 3 && cube.getTopFace()[1][2] == 6) {
                cube.turnUp();
                leftSolve(cube);
            }
            if (cube.getBackFace()[2][1] == 3 && cube.getTopFace()[0][1] == 6) {
                cube.turnUp();
                cube.turnUp();
                leftSolve(cube);
            }

            // greenorange
            if (cube.getLeftFace()[0][1] == 6 && cube.getTopFace()[1][0] == 3) {
                rightSolveLeft(cube);

            }
            if (cube.getFrontFace()[0][1] == 6 && cube.getTopFace()[2][1] == 3) {

                cube.turnUp();
                rightSolveLeft(cube);
            }
            if (cube.getBackFace()[2][1] == 6 && cube.getTopFace()[0][1] == 3) {
                cube.turnUpPrime();
                rightSolveLeft(cube);
            }
            if (cube.getRightFace()[0][1] == 6 && cube.getTopFace()[1][2] == 3) {

                cube.turnUp();
                cube.turnUp();
                rightSolveLeft(cube);

            }

            // greenred
            if (cube.getLeftFace()[0][1] == 6 && cube.getTopFace()[1][0] == 4) {

                leftSolveLeft(cube);

            }
            if (cube.getFrontFace()[0][1] == 6 && cube.getTopFace()[2][1] == 4) {

                cube.turnUp();
                leftSolveLeft(cube);

            }
            if (cube.getBackFace()[2][1] == 6 && cube.getTopFace()[0][1] == 4) {

                cube.turnUpPrime();
                leftSolveLeft(cube);

            }
            if (cube.getRightFace()[0][1] == 6 && cube.getTopFace()[1][2] == 4) {

                cube.turnUp();
                cube.turnUp();
                leftSolveLeft(cube);

            }

            // blueorange
            if (cube.getRightFace()[0][1] == 5 && cube.getTopFace()[1][2] == 3) {

                leftSolveRight(cube);

            }
            if (cube.getBottomFace()[2][1] == 5 && cube.getTopFace()[0][1] == 3) {

                cube.turnUp();
                leftSolveRight(cube);

            }
            if (cube.getFrontFace()[0][1] == 5 && cube.getTopFace()[2][1] == 3) {

                cube.turnUpPrime();
                leftSolveRight(cube);

            }
            if (cube.getLeftFace()[0][1] == 5 && cube.getTopFace()[1][0] == 3) {

                cube.turnUp();
                cube.turnUp();
                leftSolveRight(cube);

            }

            // bluered
            if (cube.getRightFace()[0][1] == 5 && cube.getTopFace()[1][2] == 4) {

                rightSolveRight(cube);

            }
            if (cube.getBottomFace()[2][1] == 5 && cube.getTopFace()[0][1] == 4) {

                cube.turnUp();
                rightSolveRight(cube);

            }
            if (cube.getFrontFace()[0][1] == 5 && cube.getTopFace()[2][1] == 4) {

                cube.turnUpPrime();
                rightSolveRight(cube);

            }
            if (cube.getLeftFace()[0][1] == 5 && cube.getTopFace()[1][0] == 4) {

                cube.turnUp();
                cube.turnUp();
                rightSolveRight(cube);

            }

            // redblue
            if (cube.getBottomFace()[1][2] == 4 && cube.getTopFace()[0][1] == 5) {

                leftSolveBack(cube);

            }
            if (cube.getLeftFace()[0][1] == 4 && cube.getTopFace()[1][0] == 5) {

                cube.turnUp();
                leftSolveBack(cube);

            }
            if (cube.getRightFace()[0][1] == 4 && cube.getTopFace()[1][2] == 5) {

                cube.turnUpPrime();
                leftSolveBack(cube);

            }
            if (cube.getFrontFace()[0][1] == 4 && cube.getTopFace()[2][1] == 5) {

                cube.turnUp();
                cube.turnUp();
                leftSolveBack(cube);

            }

            // redgreen
            if (cube.getBottomFace()[1][2] == 4 && cube.getTopFace()[0][1] == 6) {

                rightSolveBack(cube);

            }
            if (cube.getLeftFace()[0][1] == 4 && cube.getTopFace()[1][0] == 6) {

                cube.turnUp();
                rightSolveBack(cube);

            }
            if (cube.getRightFace()[0][1] == 4 && cube.getTopFace()[1][2] == 6) {

                cube.turnUpPrime();
                rightSolveBack(cube);

            }
            if (cube.getFrontFace()[0][1] == 4 && cube.getTopFace()[2][1] == 6) {

                cube.turnUp();
                cube.turnUp();
                rightSolveBack(cube);

            }

            if (cube.getFrontFace()[1][2] == 5 && cube.getRightFace()[1][0] == 3) {
                rightSolve(cube);
            }
            if (cube.getFrontFace()[1][0] == 6 && cube.getLeftFace()[1][2] == 3) {
                leftSolve(cube);
            }
            if (cube.getLeftFace()[1][0] == 4 && cube.getBackFace()[1][0] == 6) {

                leftSolveLeft(cube);

            }
            if (cube.getRightFace()[1][2] == 4 && cube.getBottomFace()[1][2] == 5) {

                rightSolveRight(cube);

            }
            // if the loop gets stuck, it enters here and kicks a piece out of
            // its spot
            if (count % 100 == 0) {

                if (cube.getFrontFace()[1][0] != 3) {
                    leftSolve(cube);
                }
                if (cube.getFrontFace()[1][2] != 3) {
                    rightSolve(cube);
                }
                if (cube.getRightFace()[1][0] != 5) {
                    leftSolveRight(cube);
                }
                if (cube.getRightFace()[1][2] != 5) {
                    rightSolveRight(cube);
                }
                if (cube.getLeftFace()[1][0] != 6) {
                    leftSolveLeft(cube);
                }
                if (cube.getLeftFace()[1][2] != 6) {
                    rightSolveLeft(cube);
                }
                if (cube.getBackFace()[1][0] != 4) {
                    rightSolveBack(cube);
                }
                if (cube.getBackFace()[1][2] != 4) {
                    leftSolveBack(cube);
                }
            }

        }

    }

    // Here is a helper for solve the top cross
    public static void TopCrossHelper(Cube cube) {
        cube.turnFront();
        cube.turnRight();
        cube.turnUpPrime();
        cube.turnRightPrime();
        cube.turnFrontPrime();
        cube.turnLeftPrime();
        cube.turnUp();
        cube.turnLeft();
    }

    /**
     * This part of the program solves the top yellow cross. It is a lot shorter
     * than the other methods as there are only a few cases.
     *
     * @param cube
     */

    static public void solveTopCross(Cube cube) {
        int count = 0;
        while (1 == 1) {

            if (count > 100) {
                break;
            }
            count++;
            // if the top cross is solved, it breaks
            if (cube.getTopFace()[0][1] == 2 && cube.getTopFace()[1][0] == 2 && cube.getTopFace()[1][2] == 2
                    && cube.getTopFace()[2][1] == 2) {
                break;
            }

            // does algorithm depending on situation
            if (cube.getTopFace()[0][1] == 2) {
                if (cube.getTopFace()[1][2] == 2) {
                    TopCrossHelper(cube);
                } else {
                    cube.turnUp();
                    TopCrossHelper(cube);
                }
            } else if (cube.getTopFace()[2][1] == 2) {
                if (cube.getTopFace()[1][0] == 2) {
                    cube.turnUp();
                    cube.turnUp();
                    TopCrossHelper(cube);
                } else {
                    cube.turnUp();
                    TopCrossHelper(cube);
                }

            } else {
                TopCrossHelper(cube);
                count++;
            }
        }

    }

	/*
	 * These two helper methods are to complete the algorithm sune and the anti
	 * sune. They are called sune and leftsune to avoid confusion, and to better
	 * clarify which one is used.
	 */

    public static void Sune(Cube cube) {
        cube.turnRight();
        cube.turnUp();
        cube.turnRightPrime();
        cube.turnUp();
        cube.turnRight();
        cube.turnUp();
        cube.turnUp();
        cube.turnRightPrime();
    }

    public static void LeftSune(Cube cube) {
        cube.turnLeftPrime();
        cube.turnUpPrime();
        cube.turnLeft();
        cube.turnUpPrime();
        cube.turnLeftPrime();
        cube.turnUpPrime();
        cube.turnUpPrime();
        cube.turnLeft();
    }

    /**
     * This method solves the top face of the cube. Similar to the last method,
     * there are only about 8 different cases, so creating this was very simple
     * compared to the other methods.
     *
     * @param cube
     */
    static public void solveTopFace(Cube cube) {
        int count = 0;
        while (1 == 1) {
            if (count > 100) {
                break;
            }
            count++;
            // in the case where it is already solved
            if (cube.getTopFace()[0][0] == 2 && cube.getTopFace()[0][2] == 2 && cube.getTopFace()[2][0] == 2
                    && cube.getTopFace()[2][2] == 2) {
                break;
            }

            // does right sune and breaks
            if (cube.getTopFace()[2][0] == 2 && cube.getFrontFace()[0][2] == 2 && cube.getTopFace()[0][2] != 2
                    && cube.getTopFace()[0][0] != 2) {
                Sune(cube);
                break;
            }

            // does left sune and breaks
            if (cube.getTopFace()[2][2] == 2 && cube.getFrontFace()[0][0] == 2 && cube.getTopFace()[0][0] != 2
                    && cube.getTopFace()[0][2] != 2) {
                LeftSune(cube);
                break;
            }
            // harder cases
            if (cube.getTopFace()[0][0] != 2 && cube.getTopFace()[0][2] != 2 && cube.getTopFace()[2][0] != 2
                    && cube.getTopFace()[2][2] != 2) {
                if (cube.getLeftFace()[0][0] == 2 && cube.getLeftFace()[0][2] == 2) {
                    Sune(cube);

                } else {
                    while ((cube.getLeftFace()[0][0] != 2 && cube.getLeftFace()[0][2] != 2)) {
                        cube.turnUp();
                    }
                    Sune(cube);
                }
            }
            if (cube.getTopFace()[0][2] == 2 && cube.getTopFace()[2][2] == 2) {
                if (cube.getFrontFace()[0][0] == 2) {
                    Sune(cube);
                } else {
                    cube.turnUpPrime();
                    Sune(cube);
                }
            }
            // last two cases
            if (cube.getTopFace()[0][0] == 2 && cube.getTopFace()[2][2] == 2 && cube.getFrontFace()[0][0] == 2) {
                Sune(cube);
            }
            if (cube.getTopFace()[0][0] == 2 && cube.getTopFace()[0][2] == 2) {
                if (cube.getFrontFace()[0][0] == 2) {
                    Sune(cube);
                } else {
                    cube.turnUp();
                    Sune(cube);
                }

            }
            // continues to turn up until a case it met
            cube.turnUp();
            count++;
        }

    }

    // this method helps solve the corners
    public static void CornerHelper(Cube cube) {
        cube.turnRightPrime();
        cube.turnFront();
        cube.turnRightPrime();
        cube.turnBack();
        cube.turnBack();
        cube.turnRight();
        cube.turnFrontPrime();
        cube.turnRightPrime();
        cube.turnBack();
        cube.turnBack();
        cube.turnRight();
        cube.turnRight();
    }

    /**
     * This method aligns the corners of the cube. This is one of the last steps
     * and just like many of the other steps, it does not require too many
     * different cases.
     */
    static public void alignCorners(Cube cube) {
        int count = 0;
        int count2 = 0;

        while (1 == 1) {
            if (count2 > 100) {
                break;
            }

            // in the case that they are already aligned, break
            if (cube.getFrontFace()[0][0] == cube.getFrontFace()[0][2]
                    && cube.getRightFace()[0][0] == cube.getRightFace()[0][2]
                    && cube.getLeftFace()[0][0] == cube.getLeftFace()[0][2]) {
                break;
            }

            if (cube.getFrontFace()[0][0] == cube.getRightFace()[0][2]) {
                if (cube.getRightFace()[0][0] == cube.getBackFace()[2][0]) {
                    cube.turnUp();
                    CornerHelper(cube);
                    break;
                }
                CornerHelper(cube);
                break;
            }
            cube.turnUp();
            count++;

			/*
			 * In a weird case where the edges are not aligned at all, it will
			 * do the helper method after 4 times, and then will be able to
			 * solve it. This is a rare case.
			 */
            if (count == 4) {
                count = 0;
                count2++;
                CornerHelper(cube);
            }

        }
    }

    /**
     * This is the last step of solving the cube. It aligns the centers, and
     * then completes the solve by making turns of the top face until it is
     * complete
     *
     * @param cube
     */

    static public void finalStep(Cube cube) {
    int count = 0;
        // If the centers are aligned, then turn the top face until solved
        if (cube.getFrontFace()[0][0] == cube.getFrontFace()[0][1]
                && cube.getRightFace()[0][0] == cube.getRightFace()[0][1]) {
            while (cube.getFrontFace()[0][0] != cube.getFrontFace()[1][0]) {
                count++;
                if(count>100){
                    break;
                }
                cube.turnUp();
            }
            return;
        }

        if (cube.getFrontFace()[0][1] == cube.getRightFace()[0][0]
                && cube.getRightFace()[0][1] == cube.getFrontFace()[0][0]) {

            Sune(cube);
            cube.turnUp();
            LeftSune(cube);

        }

        if (cube.getFrontFace()[0][1] == cube.getLeftFace()[0][0]
                && cube.getLeftFace()[0][1] == cube.getFrontFace()[0][0]) {
            cube.turnUp();
            Sune(cube);
            cube.turnUp();
            LeftSune(cube);

        }

        if (cube.getFrontFace()[0][1] == cube.getBackFace()[2][0]
                && cube.getBackFace()[2][1] == cube.getFrontFace()[0][0]) {
            Sune(cube);
            cube.turnUp();
            LeftSune(cube);

        }

        while (cube.getBackFace()[2][0] != cube.getBackFace()[2][1]) {
            count++;
            if(count>100){
                break;
            }
            cube.turnUp();
        }
        if (cube.getFrontFace()[0][1] == cube.getRightFace()[0][0]) {
            LeftSune(cube);
            cube.turnUpPrime();
            Sune(cube);
        } else {
            Sune(cube);
            cube.turnUp();
            LeftSune(cube);
        }

        // finishes the solve
        while (cube.getBackFace()[2][0] != cube.getBackFace()[1][1]) {
            count++;
            if(count>100){
                break;
            }
            cube.turnUp();

        }
    }

    /**
     * This method helps to reduce the size of the final arraylist by getting
     * rid of the cases where a move is undone
     *
     * @param cube
     */
    static public void reduceSize(Cube cube) {

        for (int i = 0; i < cube.getResult().size(); i++) {
            try {
                cube.getResult().get(i + 1);
            } catch (IndexOutOfBoundsException e) {

                break;
            }
            if (cube.getResult().get(i) == "U" && cube.getResult().get(i + 1) == "U'"
                    || cube.getResult().get(i) == "D" && cube.getResult().get(i + 1) == "D'"
                    || cube.getResult().get(i) == "D'" && cube.getResult().get(i + 1) == "D"
                    || cube.getResult().get(i) == "B" && cube.getResult().get(i + 1) == "B'"
                    || cube.getResult().get(i) == "B'" && cube.getResult().get(i + 1) == "B"
                    || cube.getResult().get(i) == "U'" && cube.getResult().get(i + 1) == "U"

                    ) {
                cube.getResult().remove(i);
                cube.getResult().remove(i);

            }
        }
    }

    /**
     * Like the previous method, this method reduces the size of the list.
     * However, this time it sees if there are cases where three moves in a row
     * are done, and replaces all of them with the opposite (three rights make a
     * left).
     *
     * @param cube
     */
    static public void reduceSize2(Cube cube) {
        for (int i = 0; i < cube.getResult().size(); i++) {
            try {
                cube.getResult().get(i + 3);
            } catch (IndexOutOfBoundsException e) {

                break;
            }
            if (cube.getResult().get(i) == "B" && cube.getResult().get(i + 1) == "B"
                    && cube.getResult().get(i + 2) == "B") {
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().add(i, "B'");

            }

            if (cube.getResult().get(i) == "U" && cube.getResult().get(i + 1) == "U"
                    && cube.getResult().get(i + 2) == "U") {
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().add(i, "U'");

            }

            if (cube.getResult().get(i) == "U'" && cube.getResult().get(i + 1) == "U'"
                    && cube.getResult().get(i + 2) == "U'") {
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().add(i, "U");

            }

            if (cube.getResult().get(i) == "D" && cube.getResult().get(i + 1) == "D"
                    && cube.getResult().get(i + 2) == "D") {
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().add(i, "D'");

            }

        }

    }

    /**
     * This method, like the previous two help to make the final arraylist
     * smaller. For this one, if there are two moves in a row that are the same,
     * it replaces it with a 2 of that. For example U-U gets replaced with U2.
     *
     * @param cube
     */

    static public void reduceSize3(Cube cube) {

        for (int i = 0; i < cube.getResult().size(); i++) {
            try {
                cube.getResult().get(i + 1);
            } catch (IndexOutOfBoundsException e) {

                break;
            }
            if (cube.getResult().get(i) == "U" && cube.getResult().get(i + 1) == "U"
                    || cube.getResult().get(i) == "U'" && cube.getResult().get(i + 1) == "U'") {
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().add(i, "U2");
            }
            if (cube.getResult().get(i) == "R" && cube.getResult().get(i + 1) == "R"
                    || cube.getResult().get(i) == "R'" && cube.getResult().get(i + 1) == "R'") {
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().add(i, "R2");
            }
            if (cube.getResult().get(i) == "D" && cube.getResult().get(i + 1) == "D"
                    || cube.getResult().get(i) == "D'" && cube.getResult().get(i + 1) == "D'") {
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().add(i, "D2");
            }
            if (cube.getResult().get(i) == "F" && cube.getResult().get(i + 1) == "F") {
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().add(i, "F2");
            }
            if (cube.getResult().get(i) == "B" && cube.getResult().get(i + 1) == "B"
                    || cube.getResult().get(i) == "B'" && cube.getResult().get(i + 1) == "B'") {
                cube.getResult().remove(i);
                cube.getResult().remove(i);
                cube.getResult().add(i, "B2");
            }
        }
    }

    /**
     * This method compiles all of the previous methods and puts them into one
     * and then returns the final arraylist.
     *
     * @param cube
     * @return
     */
    static public ArrayList<String> solveCube(Cube cube) {

        cube.startingCube();
        Solve.solveCross(cube);
        cube.cross();
        Solve.solveFirstLayer(cube);
        cube.firstLayer();
        Solve.solveSecondLayer(cube);
        cube.secondLayer();
        Solve.solveTopCross(cube);
        cube.topCross();
        Solve.solveTopFace(cube);
        cube.topFace();
        Solve.alignCorners(cube);
        cube.alignCorners();
        Solve.finalStep(cube);
        cube.finalStep();
        int i = 0;
        int j = 0;
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                if (cube.getFrontFace()[i][j] != 3) {
                    cube.notValid();
                    break;
                }
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                if (cube.getBackFace()[i][j] != 4) {
                    cube.notValid();
                    break;
                }
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                if (cube.getTopFace()[i][j] != 2) {
                    cube.notValid();
                    break;
                }
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                if (cube.getBottomFace()[i][j] != 1) {
                    cube.notValid();
                    break;
                }
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                if (cube.getRightFace()[i][j] != 5) {
                    cube.notValid();
                    break;
                }
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                if (cube.getLeftFace()[i][j] != 6) {
                    cube.notValid();
                    break;
                }
            }
        }

        for (int m = 0; m < 5; m++) {
            Solve.reduceSize(cube);
            Solve.reduceSize(cube);
            Solve.reduceSize2(cube);
        }
        Solve.reduceSize3(cube);

        // return the final array list
        return cube.getResult();
    }
}