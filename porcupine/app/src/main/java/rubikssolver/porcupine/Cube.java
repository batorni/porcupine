package rubikssolver.porcupine;
import java.util.ArrayList;

/**
 * This is the cube class that contains all of the populating of the cube and
 * turning methods. Every single possible move of the cube is in this class. It
 * communicates with the solve class to solve the cube.
 */

public class Cube {

    // we will assume that yellow is on top, orange facing you, white on bottom

    // 1 is white
    // 2 is yellow
    // 3 is orange
    // 4 is red
    // 5 is blue
    // 6 is green

    private ArrayList<String> SolvedArray = new ArrayList<String>();
    private int[][] FrontFace;
    private int[][] BackFace;
    private int[][] TopFace;
    private int[][] LeftFace;
    private int[][] RightFace;
    private int[][] BottomFace;

    public Cube(int[][] FrontFace, int[][] BackFace, int[][] TopFace, int[][] LeftFace, int[][] RightFace,
                int[][] BottomFace) {

        this.FrontFace = FrontFace;
        this.BackFace = BackFace;
        this.TopFace = TopFace;
        this.LeftFace = LeftFace;
        this.RightFace = RightFace;
        this.BottomFace = BottomFace;
    }

    /**
     * This method makes a temp face identical to the regular face. This is
     * helpful for turning because I do not want anything overwritten.
     *
     * @param face
     * @return temp array
     */
    public int[][] makeTempFace(int[][] face) {

        int[][] temp = new int[3][3];
        for (int i = 0; i < face.length; i++) {
            for (int j = 0; j < face[i].length; j++) {
                temp[i][j] = face[i][j];
            }
        }
        return temp;

    }

    /*
     * These next few methods are called in the Solve class when they are needed
     */
    public void startingCube() {
        SolvedArray.add("Beginning Solve");
    }

    public void cross() {
        SolvedArray.add("Cross Solved");
    }

    public void firstLayer() {
        SolvedArray.add("First Layer Solved");
    }

    public void secondLayer() {
        SolvedArray.add("Second Layer Solved");
    }

    public void topCross() {
        SolvedArray.add("Top Cross Solved");
    }

    public void topFace() {
        SolvedArray.add("Top Face Solved");
    }

    public void alignCorners() {
        SolvedArray.add("Corners Aligned");
    }

    public void finalStep() {
        SolvedArray.add("Cube Solved");
    }

    public void notValid() {
        SolvedArray.clear();
        SolvedArray.add("Invalid Cube");
    }

    /**
     * These next methods simply return the instances of each of the faces. This
     * is useful for the solve class
     *
     * @return
     */
    public ArrayList<String> getResult() {
        return this.SolvedArray;
    }

    public int[][] getFrontFace() {
        return this.FrontFace;
    }

    public int[][] getBackFace() {
        return this.BackFace;
    }

    public int[][] getTopFace() {
        return this.TopFace;
    }

    public int[][] getLeftFace() {
        return this.LeftFace;
    }

    public int[][] getRightFace() {
        return this.RightFace;
    }

    public int[][] getBottomFace() {
        return this.BottomFace;
    }

    /**
     * This method makes a right turn of the cube. After it makes a turn, it
     * adds the letter R to the arraylist
     */

    public void turnRight() {

        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            FrontFace[i][2] = BottomFacetemp[i][2];
            TopFace[i][2] = FrontFacetemp[i][2];
            BackFace[i][2] = TopFacetemp[i][2];
            BottomFace[i][2] = BackFacetemp[i][2];
        }

        // realign right face
        RightFace[0][0] = RightFacetemp[2][0];
        RightFace[0][1] = RightFacetemp[1][0];
        RightFace[0][2] = RightFacetemp[0][0];
        RightFace[1][0] = RightFacetemp[2][1];
        RightFace[1][2] = RightFacetemp[0][1];
        RightFace[2][0] = RightFacetemp[2][2];
        RightFace[2][1] = RightFacetemp[1][2];
        RightFace[2][2] = RightFacetemp[0][2];

        SolvedArray.add("R");

    }

    /**
     * This method makes a right prime turn of the cube. After it makes a turn,
     * it adds the letter R' to the arraylist
     */

    public void turnRightPrime() {
        // done
        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            FrontFace[i][2] = TopFacetemp[i][2];
            TopFace[i][2] = BackFacetemp[i][2];
            BackFace[i][2] = BottomFacetemp[i][2];
            BottomFace[i][2] = FrontFacetemp[i][2];
        }

        // realign right face
        RightFace[0][0] = RightFacetemp[0][2];
        RightFace[0][1] = RightFacetemp[1][2];
        RightFace[0][2] = RightFacetemp[2][2];
        RightFace[1][0] = RightFacetemp[0][1];
        RightFace[1][2] = RightFacetemp[2][1];
        RightFace[2][0] = RightFacetemp[0][0];
        RightFace[2][1] = RightFacetemp[1][0];
        RightFace[2][2] = RightFacetemp[2][0];

        SolvedArray.add("R'");

    }

    /**
     * This method makes a left turn of the cube. After it makes a turn, it adds
     * the letter L to the arraylist
     */

    public void turnLeft() {
        // done
        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            FrontFace[i][0] = TopFacetemp[i][0];
            TopFace[i][0] = BackFacetemp[i][0];
            BackFace[i][0] = BottomFacetemp[i][0];
            BottomFace[i][0] = FrontFacetemp[i][0];
        }

        // realign left face
        LeftFace[0][0] = LeftFacetemp[2][0];
        LeftFace[0][1] = LeftFacetemp[1][0];
        LeftFace[0][2] = LeftFacetemp[0][0];
        LeftFace[1][0] = LeftFacetemp[2][1];
        LeftFace[1][2] = LeftFacetemp[0][1];
        LeftFace[2][0] = LeftFacetemp[2][2];
        LeftFace[2][1] = LeftFacetemp[1][2];
        LeftFace[2][2] = LeftFacetemp[0][2];

        SolvedArray.add("L");

    }

    /**
     * This method makes a left prime turn of the cube. After it makes a turn,
     * it adds the letter L' to the arraylist
     */

    public void turnLeftPrime() {
        // done
        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            FrontFace[i][0] = BottomFacetemp[i][0];
            TopFace[i][0] = FrontFacetemp[i][0];
            BackFace[i][0] = TopFacetemp[i][0];
            BottomFace[i][0] = BackFacetemp[i][0];
        }

        // realign left face
        LeftFace[0][0] = LeftFacetemp[0][2];
        LeftFace[0][1] = LeftFacetemp[1][2];
        LeftFace[0][2] = LeftFacetemp[2][2];
        LeftFace[1][0] = LeftFacetemp[0][1];
        LeftFace[1][2] = LeftFacetemp[2][1];
        LeftFace[2][0] = LeftFacetemp[0][0];
        LeftFace[2][1] = LeftFacetemp[1][0];
        LeftFace[2][2] = LeftFacetemp[2][0];

        SolvedArray.add("L'");

    }

    /**
     * This method makes a back turn of the cube. After it makes a turn, it adds
     * the letter B to the arraylist
     */

    public void turnBack() {
        // done

        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            RightFace[i][2] = BottomFacetemp[2][2 - i];
            TopFace[0][i] = RightFacetemp[i][2];
            LeftFace[2 - i][0] = TopFacetemp[0][i];
            BottomFace[2][i] = LeftFacetemp[i][0];
        }

        // realign back face
        BackFace[0][0] = BackFacetemp[2][0];
        BackFace[0][1] = BackFacetemp[1][0];
        BackFace[0][2] = BackFacetemp[0][0];
        BackFace[1][0] = BackFacetemp[2][1];
        BackFace[1][2] = BackFacetemp[0][1];
        BackFace[2][0] = BackFacetemp[2][2];
        BackFace[2][1] = BackFacetemp[1][2];
        BackFace[2][2] = BackFacetemp[0][2];

        SolvedArray.add("B");

    }

    /**
     * This method makes a back prime turn of the cube. After it makes a turn,
     * it adds the letter B' to the arraylist
     */

    public void turnBackPrime() {
        // done

        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            RightFace[i][2] = TopFacetemp[0][i];
            TopFace[0][2 - i] = LeftFacetemp[i][0];
            LeftFace[i][0] = BottomFacetemp[2][i];
            BottomFace[2][2 - i] = RightFacetemp[i][2];
        }

        // realign back face
        BackFace[0][0] = BackFacetemp[0][2];
        BackFace[0][1] = BackFacetemp[1][2];
        BackFace[0][2] = BackFacetemp[2][2];
        BackFace[1][0] = BackFacetemp[0][1];
        BackFace[1][2] = BackFacetemp[2][1];
        BackFace[2][0] = BackFacetemp[0][0];
        BackFace[2][1] = BackFacetemp[1][0];
        BackFace[2][2] = BackFacetemp[2][0];

        SolvedArray.add("B'");

    }

    /**
     * This method makes a down turn of the cube. After it makes a turn, it adds
     * the letter D to the arraylist
     */

    public void turnDown() { // for what
        // done
        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            FrontFace[2][i] = LeftFacetemp[2][i];
            LeftFace[2][i] = BackFacetemp[0][2 - i];
            BackFace[0][2 - i] = RightFacetemp[2][i];
            RightFace[2][i] = FrontFacetemp[2][i];
        }

        // realign bottom face
        BottomFace[0][0] = BottomFacetemp[2][0];
        BottomFace[0][1] = BottomFacetemp[1][0];
        BottomFace[0][2] = BottomFacetemp[0][0];
        BottomFace[1][0] = BottomFacetemp[2][1];
        BottomFace[1][2] = BottomFacetemp[0][1];
        BottomFace[2][0] = BottomFacetemp[2][2];
        BottomFace[2][1] = BottomFacetemp[1][2];
        BottomFace[2][2] = BottomFacetemp[0][2];

        SolvedArray.add("D");

    }

    /**
     * This method makes a down prime turn of the cube. After it makes a turn,
     * it adds the letter D' to the arraylist
     */

    public void turnDownPrime() {
        // done
        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            FrontFace[2][i] = RightFacetemp[2][i];
            LeftFace[2][i] = FrontFacetemp[2][i];
            BackFace[0][2 - i] = LeftFacetemp[2][i];
            RightFace[2][i] = BackFacetemp[0][2 - i];
        }

        // realign bottom face
        BottomFace[0][0] = BottomFacetemp[0][2];
        BottomFace[0][1] = BottomFacetemp[1][2];
        BottomFace[0][2] = BottomFacetemp[2][2];
        BottomFace[1][0] = BottomFacetemp[0][1];
        BottomFace[1][2] = BottomFacetemp[2][1];
        BottomFace[2][0] = BottomFacetemp[0][0];
        BottomFace[2][1] = BottomFacetemp[1][0];
        BottomFace[2][2] = BottomFacetemp[2][0];

        SolvedArray.add("D'");

    }

    /**
     * This method makes a front turn of the cube. After it makes a turn, it
     * adds the letter F to the arraylist
     */

    public void turnFront() {
        // done
        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            TopFace[2][i] = LeftFacetemp[2 - i][2];
            LeftFace[i][2] = BottomFacetemp[0][i];
            BottomFace[0][i] = RightFacetemp[2 - i][0];
            RightFace[i][0] = TopFacetemp[2][i];
        }

        // realign front face
        FrontFace[0][0] = FrontFacetemp[2][0];
        FrontFace[0][1] = FrontFacetemp[1][0];
        FrontFace[0][2] = FrontFacetemp[0][0];
        FrontFace[1][0] = FrontFacetemp[2][1];
        FrontFace[1][2] = FrontFacetemp[0][1];
        FrontFace[2][0] = FrontFacetemp[2][2];
        FrontFace[2][1] = FrontFacetemp[1][2];
        FrontFace[2][2] = FrontFacetemp[0][2];

        SolvedArray.add("F");

    }

    /**
     * This method makes a front prime turn of the cube. After it makes a turn,
     * it adds the letter F' to the arraylist
     */

    public void turnFrontPrime() {
        // done
        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] BottomFacetemp = makeTempFace(BottomFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            TopFace[2][i] = RightFacetemp[i][0];
            LeftFace[i][2] = TopFacetemp[2][2 - i];
            BottomFace[0][i] = LeftFacetemp[i][2];
            RightFace[i][0] = BottomFacetemp[0][2 - i];
        }

        // realign front face
        FrontFace[0][0] = FrontFacetemp[0][2];
        FrontFace[0][1] = FrontFacetemp[1][2];
        FrontFace[0][2] = FrontFacetemp[2][2];
        FrontFace[1][0] = FrontFacetemp[0][1];
        FrontFace[1][2] = FrontFacetemp[2][1];
        FrontFace[2][0] = FrontFacetemp[0][0];
        FrontFace[2][1] = FrontFacetemp[1][0];
        FrontFace[2][2] = FrontFacetemp[2][0];

        SolvedArray.add("F'");

    }

    /**
     * This method makes an up turn of the cube. After it makes a turn, it adds
     * the letter U to the arraylist
     */

    public void turnUp() {
        // done
        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            FrontFace[0][i] = RightFacetemp[0][i];
            RightFace[0][i] = BackFacetemp[2][2 - i];
            BackFace[2][2 - i] = LeftFacetemp[0][i];
            LeftFace[0][i] = FrontFacetemp[0][i];
        }

        // realign top face
        TopFace[0][0] = TopFacetemp[2][0];
        TopFace[0][1] = TopFacetemp[1][0];
        TopFace[0][2] = TopFacetemp[0][0];
        TopFace[1][0] = TopFacetemp[2][1];
        TopFace[1][2] = TopFacetemp[0][1];
        TopFace[2][0] = TopFacetemp[2][2];
        TopFace[2][1] = TopFacetemp[1][2];
        TopFace[2][2] = TopFacetemp[0][2];

        SolvedArray.add("U");

    }

    /**
     * This method makes a up prime turn of the cube. After it makes a turn, it
     * adds the letter U' to the arraylist
     */

    public void turnUpPrime() {
        // done
        int[][] FrontFacetemp = makeTempFace(FrontFace);
        int[][] BackFacetemp = makeTempFace(BackFace);
        int[][] TopFacetemp = makeTempFace(TopFace);
        int[][] RightFacetemp = makeTempFace(RightFace);
        int[][] LeftFacetemp = makeTempFace(LeftFace);

        // swap faces
        for (int i = 0; i <= 2; i++) {
            FrontFace[0][i] = LeftFacetemp[0][i];
            RightFace[0][i] = FrontFacetemp[0][i];
            BackFace[2][2 - i] = RightFacetemp[0][i];
            LeftFace[0][i] = BackFacetemp[2][2 - i];
        }

        // realign top face
        TopFace[0][0] = TopFacetemp[0][2];
        TopFace[0][1] = TopFacetemp[1][2];
        TopFace[0][2] = TopFacetemp[2][2];
        TopFace[1][0] = TopFacetemp[0][1];
        TopFace[1][2] = TopFacetemp[2][1];
        TopFace[2][0] = TopFacetemp[0][0];
        TopFace[2][1] = TopFacetemp[1][0];
        TopFace[2][2] = TopFacetemp[2][0];

        SolvedArray.add("U'");

    }

}