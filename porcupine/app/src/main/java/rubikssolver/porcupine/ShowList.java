package rubikssolver.porcupine;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import android.app.Activity;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import java.util.ArrayList;

public class ShowList extends AppCompatActivity {

    GridView grid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list);

        ArrayList<String> list = Storage.getInstance().getSolution();

//
//        ArrayList<String> list = new ArrayList<String>();
//
//        String str = "a";
//        for(int i = 0; i < 50; i ++) {
//            list.add(str);
//
//            str += "S";
//        }

        grid = (GridView) findViewById(R.id.gridview);
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        grid.setAdapter(adapter);
    }

}
