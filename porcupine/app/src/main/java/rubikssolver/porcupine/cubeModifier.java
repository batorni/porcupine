package rubikssolver.porcupine;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class cubeModifier extends AppCompatActivity {

    int[] colors;
    int[][] face;
    Button [] buttons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cube_modifier);

        colors = new int[6];

        colors[0] = Color.WHITE;
        colors[1] = Color.YELLOW;
        colors[2] = -26368;
        colors[3] = Color.RED;
        colors[4] = Color.BLUE;
        colors[5] = Color.GREEN;

        face = Storage.getInstance().getCurrentFace();


        buttons = new Button[9];
        int count = 0;
        for(int i = 0; i < 13; i++) {
            if((R.id.topLeft + i) != R.id.line1 && (R.id.topLeft + i) != R.id.line2 &&
                    (R.id.topLeft + i) != R.id.line3 && (R.id.topLeft + i) != R.id.line4) {
                buttons[count] = (Button) findViewById(R.id.topLeft + i);
                count ++;
            }
        }

        int index = 0;
        for(int x = 0; x < 3; x++) {
            for(int y = 0; y < 3; y++) {
                buttons[index].setBackgroundColor(colors[(face[x][y]) - 1]);
                index ++;
            }
        }

    }

    public void onButtonClick(View view) {

        int buttonNum = view.getId() - R.id.topLeft;

        if(buttonNum == 4) {
            return;
        }

        int count = 0;
        for(int x = 0; x < 3; x++) {
            for(int y = 0; y < 3; y++) {
                if(count == buttonNum) {
                    if(face[x][y] != 6) {
                        face[x][y] = (face[x][y] + 1);
                    } else {
                        face[x][y] = 1;
                    }

                    //this is needed for the oddities that occur from the black gridlines I added
                    if(count < 6) {
                        buttons[count].setBackgroundColor(colors[(face[x][y]) - 1]);
                    } else {
                        buttons[count-2].setBackgroundColor(colors[(face[x][y]) - 1]);
                    }
                }

                //this is needed for the oddities that occur from the black gridlines I added
                if(count == 5) {
                    count = 7;
                }
                count ++;
            }
        }
    }

    public void onModifierDoneClick(View view) {

        Storage.getInstance().setCurrentFace(face, Storage.getInstance().getSideNumber());

        finish();
    }
}
