package rubikssolver.porcupine;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

import java.io.ByteArrayOutputStream;
import java.util.Timer;


public class TimerActivity extends AppCompatActivity {

    Chronometer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        timer = (Chronometer) findViewById(R.id.timer);
        timer.setBase(SystemClock.elapsedRealtime());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rubikscube, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        //noinspection SimplifiableIfStatement
        switch(item.getItemId()){
            case R.id.action_settings: {
                System.out.println("action_settings was selected");
                return true;
            }
            case R.id.solver: {
                System.out.println("solver was selected");
                Intent intent = new Intent(this, SolverActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.timer: {
                System.out.println("timer was selected");
                Intent intent = new Intent(this, TimerActivity.class);
                startActivity(intent);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void onTimerClick(View view) {

        Button b = (Button) view;

        if(b.getText().toString().equals("Start")) {
            b.setText("Stop");
            b.setBackgroundColor(Color.RED);
            timer.setBase(SystemClock.elapsedRealtime());
            timer.start();

        } else {
            b.setText("Start");
            b.setBackgroundColor(Color.GREEN);

            timer.stop();
        }
    }

    public void onResetClick(View view) {

        Button b = (Button) view;
        timer.setBase(SystemClock.elapsedRealtime());

    }
}
