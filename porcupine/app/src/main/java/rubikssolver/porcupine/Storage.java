package rubikssolver.porcupine;

import java.util.ArrayList;

/**
 * Created by batorni on 11/11/2015.
 *
 * This class serves as a way to transport objects and data between activities.
 * Simply call Storate.getInstance()."whateverGetterYouWantHere" to get the object you're looking for.
 * If you would like to set an object, simply call Storage.getInstance()."whateverSetterYouWantHere"
 *
 */
public class Storage {

    private static Storage instance;

    // Empty constructor.
    private Storage() {
    }

    // Returns the current instance
    public static Storage getInstance() {
        if(instance == null) {
            instance = new Storage();
        }

        return instance;
    }

    /**
     * The following serve as getters/setters for the cube.
     * This will most likely need to be Nik's cube class
     */
    private int [][][] cube;

    public int[][][] getCube() {
        return cube;
    }

    public void setCube(int [][][] var) {
        cube = var;
    }

    /**
     * This is used for the CubeModifier/CubeFixer Classes
     */
    private int[][] currentFace;
    private int side;

    public int[][] getCurrentFace() {
        return currentFace;
    }

    public int getSideNumber() {
        return side;
    }

    public void setCurrentFace(int [][] cur, int side) {
        currentFace = cur;
        this.side = side;
    }


    /**
     * Arraylist containing all the moves needed to solve the current
     * instaance of the cube
     */

    private ArrayList<String> solution;

    public ArrayList<String> getSolution() { return solution; }

    public void setSolution(ArrayList<String> list) {
        solution = list;
    }

}
