public class Solve {

    // 1 is white
    // 2 is yellow
    // 3 is orange
    // 4 is red
    // 5 is blue
    // 6 is green

    static public void solveCross(Cube cube) {
        System.out.println("hello");
        boolean Green = false;
        boolean Orange = false;
        boolean Blue = false;
        boolean Red = false;
        int count = 0;

        while (count < 100) {

    
            if (cube.getBottomFace()[0][1] == 1 && cube.getFrontFace()[2][1] == 3) {
            
                Orange = true;
            }
            if (cube.getBottomFace()[1][0] == 1 && cube.getLeftFace()[2][1] == 6) {
                Green = true;
            }
            if (cube.getBottomFace()[1][2] == 1 && cube.getRightFace()[2][1] == 5) {
                Blue = true;
            }
            if (cube.getBottomFace()[2][1] == 1 && cube.getBackFace()[2][1] == 4) {
                Red = true;
            }
            if ((Orange == true) && (Blue == true) && (Orange == true) && (Red == true)) {
                break;
            }
            
            if (cube.getFrontFace()[1][2] == 1) {
                // done
                if (cube.getRightFace()[1][0] == 5) {
                    cube.turnRightPrime();
                    Blue = true;
                } else if (cube.getRightFace()[1][0] == 3) {
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    Orange = true;
                } else if (cube.getRightFace()[1][0] == 4) {
                    cube.turnDownPrime();
                    cube.turnRightPrime();
                    cube.turnDown();
                    Red = true;
                } else if (cube.getRightFace()[1][0] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDown();
                    cube.turnDown();
                    Green = true;
                }

            }
    
            if (cube.getRightFace()[1][0] == 1) {
                // done
                if (cube.getFrontFace()[1][2] == 5) {
                    cube.turnDown();
                    cube.turnFront();
                    cube.turnDownPrime();
                    Blue = true;
                } else if (cube.getFrontFace()[1][2] == 3) {
                    cube.turnFront();
                    Orange = true;
                } else if (cube.getFrontFace()[1][2] == 4) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnFront();
                    cube.turnDown();
                    cube.turnDown();
                    Red = true;
                } else if (cube.getFrontFace()[1][2] == 6) {
                    cube.turnDownPrime();
                    cube.turnFront();
                    cube.turnDown();
                    Green = true;
                }

            }

            if (cube.getFrontFace()[1][0] == 1) {
                // done
                if (cube.getLeftFace()[1][2] == 5) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnLeft();
                    cube.turnDown();
                    cube.turnDown();
                    Blue = true;
                } else if (cube.getLeftFace()[1][2] == 3) {
                    cube.turnDownPrime();
                    cube.turnLeft();
                    cube.turnDown();
                    Orange = true;
                } else if (cube.getLeftFace()[1][2] == 4) {
                    cube.turnDown();
                    cube.turnLeft();
                    cube.turnDownPrime();
                    Red = true;
                } else if (cube.getLeftFace()[1][2] == 6) {
                    cube.turnLeft();
                    Green = true;
                }

            }

            if (cube.getLeftFace()[1][2] == 1) {
                // done
                if (cube.getFrontFace()[1][0] == 5) {
                    cube.turnDownPrime();
                    cube.turnFrontPrime();
                    cube.turnDown();
                    Blue = true;
                } else if (cube.getFrontFace()[1][0] == 3) {
                    cube.turnFrontPrime();
                    Orange = true;
                } else if (cube.getFrontFace()[1][0] == 4) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnFrontPrime();
                    cube.turnDown();
                    cube.turnDown();
                    Red = true;
                } else if (cube.getFrontFace()[1][0] == 6) {
                    cube.turnDown();
                    cube.turnFrontPrime();
                    cube.turnDownPrime();
                    Green = true;
                }

            }

            if (cube.getBackFace()[1][2] == 1) {
                // done
                if (cube.getRightFace()[1][2] == 5) {
                    cube.turnRight();
                    Blue = true;
                } else if (cube.getRightFace()[1][2] == 3) {
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnDownPrime();
                    Orange = true;
                } else if (cube.getRightFace()[1][2] == 4) {
                    cube.turnDownPrime();
                    cube.turnRight();
                    cube.turnDown();
                    Red = true;
                } else if (cube.getRightFace()[1][2] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnDown();
                    cube.turnDown();
                    Green = true;
                }

            }

            if (cube.getRightFace()[1][2] == 1) {
                // done
                if (cube.getBackFace()[1][2] == 5) {
                    cube.turnDown();
                    cube.turnBackPrime();
                    cube.turnDownPrime();
                    Blue = true;
                } else if (cube.getBackFace()[1][2] == 3) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnBackPrime();
                    cube.turnDown();
                    cube.turnDown();
                    Orange = true;
                } else if (cube.getBackFace()[1][2] == 4) {
                    cube.turnBackPrime();
                    Red = true;
                } else if (cube.getBackFace()[1][2] == 6) {
                    cube.turnDownPrime();
                    cube.turnBackPrime();
                    cube.turnDown();
                    Green = true;
                }

            }

            if (cube.getBackFace()[1][0] == 1) {

                if (cube.getLeftFace()[1][0] == 5) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnLeftPrime();
                    cube.turnDown();
                    cube.turnDown();
                    Blue = true;
                } else if (cube.getLeftFace()[1][0] == 3) {
                    cube.turnDownPrime();
                    cube.turnLeftPrime();
                    cube.turnDown();
                    Orange = true;
                } else if (cube.getLeftFace()[1][0] == 4) {
                    cube.turnDown();
                    cube.turnLeftPrime();
                    cube.turnDownPrime();
                    Red = true;
                } else if (cube.getLeftFace()[1][0] == 6) {
                    cube.turnLeftPrime();
                    Green = true;
                }

            }

            if (cube.getLeftFace()[1][0] == 1) {
                // done
                if (cube.getBackFace()[1][0] == 5) {
                    cube.turnDown();
                    cube.turnBack();
                    cube.turnDownPrime();
                    Blue = true;
                } else if (cube.getBackFace()[1][0] == 3) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnBack();
                    cube.turnDown();
                    cube.turnDown();
                    Orange = true;
                } else if (cube.getBackFace()[1][0] == 4) {
                    cube.turnBack();
                    Red = true;
                } else if (cube.getBackFace()[1][0] == 6) {
                    cube.turnDownPrime();
                    cube.turnBack();
                    cube.turnDown();
                    Green = true;
                }

            }

            if (cube.getTopFace()[1][2] == 1) {
                // done
                if (cube.getRightFace()[0][1] == 5) {
                    cube.turnRight();
                    cube.turnRight();
                    Blue = true;
                } else if (cube.getRightFace()[0][1] == 3) {
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnRight();
                    cube.turnDownPrime();
                    Orange = true;
                } else if (cube.getRightFace()[0][1] == 4) {
                    cube.turnDownPrime();
                    cube.turnRight();
                    cube.turnRight();
                    cube.turnDown();
                    Red = true;
                } else if (cube.getRightFace()[0][1] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnRight();
                    cube.turnDown();
                    cube.turnDown();
                    Green = true;
                }

            }

            if (cube.getFrontFace()[0][1] == 1) {
                cube.turnUpPrime();
            } else if (cube.getLeftFace()[0][1] == 1) {
                cube.turnUp();
                cube.turnUp();
            } else if (cube.getBackFace()[2][1] == 1) {
                cube.turnUpPrime();
            }
            if (cube.getRightFace()[0][1] == 1) {
                // done
                if (cube.getTopFace()[1][2] == 5) {
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    cube.turnFront();
                    cube.turnDown();
                    Blue = true;
                } else if (cube.getTopFace()[1][2] == 3) {
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    cube.turnFront();
                    Orange = true;
                } else if (cube.getTopFace()[1][2] == 4) {
                    cube.turnDownPrime();
                    cube.turnRight();
                    cube.turnDown();
                    cube.turnBackPrime();
                    Red = true;
                } else if (cube.getTopFace()[1][2] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    cube.turnFront();
                    cube.turnDownPrime();
                    Green = true;
                }

            }

            if (cube.getTopFace()[2][1] == 1) {
                // done
                if (cube.getFrontFace()[0][1] == 5) {
                    cube.turnUpPrime();
                    cube.turnRight();
                    cube.turnRight();
                    Blue = true;
                } else if (cube.getFrontFace()[0][1] == 3) {
                    cube.turnFront();
                    cube.turnFront();
                    Orange = true;
                } else if (cube.getFrontFace()[0][1] == 4) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnBack();
                    cube.turnBack();
                    Red = true;
                } else if (cube.getFrontFace()[0][1] == 6) {
                    cube.turnUp();
                    cube.turnLeft();
                    cube.turnLeft();
                    Green = true;
                }

            }

            if (cube.getTopFace()[1][0] == 1) {
                // done
                if (cube.getLeftFace()[0][1] == 5) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnRight();
                    cube.turnRight();
                    Blue = true;
                } else if (cube.getLeftFace()[0][1] == 3) {
                    cube.turnUpPrime();
                    cube.turnFront();
                    cube.turnFront();
                    Orange = true;
                } else if (cube.getLeftFace()[0][1] == 4) {
                    cube.turnUp();
                    cube.turnBack();
                    cube.turnBack();
                    Red = true;
                } else if (cube.getLeftFace()[0][1] == 6) {
                    cube.turnLeft();
                    cube.turnLeft();
                    Green = true;
                }

            }

            if (cube.getTopFace()[0][1] == 1) {

                if (cube.getBackFace()[2][1] == 5) {
                    cube.turnUp();
                    cube.turnRight();
                    cube.turnRight();
                    Blue = true;
                } else if (cube.getBackFace()[2][1] == 3) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnFront();
                    cube.turnFront();
                    Orange = true;
                } else if (cube.getBackFace()[2][1] == 4) {

                    cube.turnBack();
                    cube.turnBack();
                    Red = true;
                } else if (cube.getBackFace()[2][1] == 6) {
                    cube.turnUpPrime();
                    cube.turnLeft();
                    cube.turnLeft();
                    Green = true;
                }

            }

            if (cube.getTopFace()[1][2] == 1) {

                if (cube.getRightFace()[0][1] == 5) {
                    cube.turnRight();
                    cube.turnRight();
                    Blue = true;
                } else if (cube.getRightFace()[0][1] == 3) {
                    cube.turnUp();
                    cube.turnFront();
                    cube.turnFront();
                    Orange = true;
                } else if (cube.getRightFace()[0][1] == 4) {
                    cube.turnUpPrime();
                    cube.turnBack();
                    cube.turnBack();
                    Red = true;
                } else if (cube.getRightFace()[0][1] == 6) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnLeft();
                    cube.turnLeft();
                    Green = true;
                }

            }

            if (cube.getFrontFace()[2][1] == 1) {
                cube.turnFront();
            }
            if (cube.getLeftFace()[2][1] == 1) {
                cube.turnLeft();
            }
            if (cube.getRightFace()[2][1] == 1) {
                cube.turnRight();
            }
            if (cube.getBackFace()[0][1] == 1) {
                cube.turnBack();
            }

            if (cube.getBottomFace()[0][1] == 1 && cube.getFrontFace()[2][1] != 3) {
                cube.turnFront();
            }
            if (cube.getBottomFace()[1][0] == 1 && cube.getLeftFace()[2][1] != 6) {
                cube.turnLeft();
            }
            if (cube.getBottomFace()[1][2] == 1 && cube.getRightFace()[2][1] != 5) {
                cube.turnRight();
            }
            if (cube.getBottomFace()[2][1] == 1 && cube.getBackFace()[0][1] != 4) {
                cube.turnBack();
            }
            count++;

        }

    }
}