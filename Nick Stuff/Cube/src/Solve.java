public class Solve {

    // 1 is white
    // 2 is yellow
    // 3 is orange
    // 4 is red
    // 5 is blue
    // 6 is green

    static public void solveCross(Cube cube) {
        boolean Green = false;
        boolean Orange = false;
        boolean Blue = false;
        boolean Red = false;
        int count = 0;

        while (count < 100) {


            if (cube.getBottomFace()[0][1] == 1 && cube.getFrontFace()[2][1] == 3 &&
            
                cube.getBottomFace()[1][0] == 1 && cube.getLeftFace()[2][1] == 6 &&
                
            cube.getBottomFace()[1][2] == 1 && cube.getRightFace()[2][1] == 5 &&
               
            cube.getBottomFace()[2][1] == 1 && cube.getBackFace()[2][1] == 4){
       System.out.println("HUH?");
                break;
            }
            
            if (cube.getFrontFace()[1][2] == 1) {
                // done
                if (cube.getRightFace()[1][0] == 5) {
                    cube.turnRightPrime();
                //    Blue = true;
                } else if (cube.getRightFace()[1][0] == 3) {
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                //    Orange = true;
                } else if (cube.getRightFace()[1][0] == 4) {
                    cube.turnDownPrime();
                    cube.turnRightPrime();
                    cube.turnDown();
                //    Red = true;
                } else if (cube.getRightFace()[1][0] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDown();
                    cube.turnDown();
                //    Green = true;
                }

            }
    
            if (cube.getRightFace()[1][0] == 1) {
                // done
                if (cube.getFrontFace()[1][2] == 5) {
                    cube.turnDown();
                    cube.turnFront();
                    cube.turnDownPrime();
                 //   Blue = true;
                } else if (cube.getFrontFace()[1][2] == 3) {
                    cube.turnFront();
                 //   Orange = true;
                } else if (cube.getFrontFace()[1][2] == 4) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnFront();
                    cube.turnDown();
                    cube.turnDown();
                 //   Red = true;
                } else if (cube.getFrontFace()[1][2] == 6) {
                    cube.turnDownPrime();
                    cube.turnFront();
                    cube.turnDown();
                 //   Green = true;
                }

            }

            if (cube.getFrontFace()[1][0] == 1) {
                // done
                if (cube.getLeftFace()[1][2] == 5) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnLeft();
                    cube.turnDown();
                    cube.turnDown();
                  //  Blue = true;
                } else if (cube.getLeftFace()[1][2] == 3) {
                    cube.turnDownPrime();
                    cube.turnLeft();
                    cube.turnDown();
                 //   Orange = true;
                } else if (cube.getLeftFace()[1][2] == 4) {
                    cube.turnDown();
                    cube.turnLeft();
                    cube.turnDownPrime();
                 //   Red = true;
                } else if (cube.getLeftFace()[1][2] == 6) {
                    cube.turnLeft();
                  //  Green = true;
                }

            }

            if (cube.getLeftFace()[1][2] == 1) {
                // done
                if (cube.getFrontFace()[1][0] == 5) {
                    cube.turnDownPrime();
                    cube.turnFrontPrime();
                    cube.turnDown();
                  //  Blue = true;
                } else if (cube.getFrontFace()[1][0] == 3) {
                    cube.turnFrontPrime();
                //    Orange = true;
                } else if (cube.getFrontFace()[1][0] == 4) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnFrontPrime();
                    cube.turnDown();
                    cube.turnDown();
                //    Red = true;
                } else if (cube.getFrontFace()[1][0] == 6) {
                    cube.turnDown();
                    cube.turnFrontPrime();
                    cube.turnDownPrime();
                //    Green = true;
                }

            }

            if (cube.getBackFace()[1][2] == 1) {
                // done
                if (cube.getRightFace()[1][2] == 5) {
                    cube.turnRight();
                //    Blue = true;
                } else if (cube.getRightFace()[1][2] == 3) {
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnDownPrime();
               //     Orange = true;
                } else if (cube.getRightFace()[1][2] == 4) {
                    cube.turnDownPrime();
                    cube.turnRight();
                    cube.turnDown();
                //    Red = true;
                } else if (cube.getRightFace()[1][2] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnDown();
                    cube.turnDown();
               //     Green = true;
                }

            }

            if (cube.getRightFace()[1][2] == 1) {
                // done
                if (cube.getBackFace()[1][2] == 5) {
                    cube.turnDown();
                    cube.turnBackPrime();
                    cube.turnDownPrime();
               //     Blue = true;
                } else if (cube.getBackFace()[1][2] == 3) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnBackPrime();
                    cube.turnDown();
                    cube.turnDown();
              //      Orange = true;
                } else if (cube.getBackFace()[1][2] == 4) {
                    cube.turnBackPrime();
              //      Red = true;
                } else if (cube.getBackFace()[1][2] == 6) {
                    cube.turnDownPrime();
                    cube.turnBackPrime();
                    cube.turnDown();
                 //   Green = true;
                }

            }

            if (cube.getBackFace()[1][0] == 1) {

                if (cube.getLeftFace()[1][0] == 5) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnLeftPrime();
                    cube.turnDown();
                    cube.turnDown();
                 //   Blue = true;
                } else if (cube.getLeftFace()[1][0] == 3) {
                    cube.turnDownPrime();
                    cube.turnLeftPrime();
                    cube.turnDown();
                 //   Orange = true;
                } else if (cube.getLeftFace()[1][0] == 4) {
                    cube.turnDown();
                    cube.turnLeftPrime();
                    cube.turnDownPrime();
               //     Red = true;
                } else if (cube.getLeftFace()[1][0] == 6) {
                    cube.turnLeftPrime();
               //     Green = true;
                }

            }

            if (cube.getLeftFace()[1][0] == 1) {
                // done
                if (cube.getBackFace()[1][0] == 5) {
                    cube.turnDown();
                    cube.turnBack();
                    cube.turnDownPrime();
              //      Blue = true;
                } else if (cube.getBackFace()[1][0] == 3) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnBack();
                    cube.turnDown();
                    cube.turnDown();
             //       Orange = true;
                } else if (cube.getBackFace()[1][0] == 4) {
                    cube.turnBack();
              //      Red = true;
                } else if (cube.getBackFace()[1][0] == 6) {
                    cube.turnDownPrime();
                    cube.turnBack();
                    cube.turnDown();
               //     Green = true;
                }

            }

            if (cube.getTopFace()[1][2] == 1) {
                // done
                if (cube.getRightFace()[0][1] == 5) {
                    cube.turnRight();
                    cube.turnRight();
                //    Blue = true;
                } else if (cube.getRightFace()[0][1] == 3) {
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnRight();
                    cube.turnDownPrime();
                //    Orange = true;
                } else if (cube.getRightFace()[0][1] == 4) {
                    cube.turnDownPrime();
                    cube.turnRight();
                    cube.turnRight();
                    cube.turnDown();
                 //   Red = true;
                } else if (cube.getRightFace()[0][1] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRight();
                    cube.turnRight();
                    cube.turnDown();
                    cube.turnDown();
                //    Green = true;
                }

            }

            if (cube.getFrontFace()[0][1] == 1) {
                cube.turnUpPrime();
            } else if (cube.getLeftFace()[0][1] == 1) {
                cube.turnUp();
                cube.turnUp();
            } else if (cube.getBackFace()[2][1] == 1) {
                cube.turnUpPrime();
            }
            if (cube.getRightFace()[0][1] == 1) {
                // done
                if (cube.getTopFace()[1][2] == 5) {
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    cube.turnFront();
                    cube.turnDown();
              //      Blue = true;
                } else if (cube.getTopFace()[1][2] == 3) {
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    cube.turnFront();
             //       Orange = true;
                } else if (cube.getTopFace()[1][2] == 4) {
                    cube.turnDownPrime();
                    cube.turnRight();
                    cube.turnDown();
                    cube.turnBackPrime();
               //     Red = true;
                } else if (cube.getTopFace()[1][2] == 6) {
                    cube.turnDown();
                    cube.turnDown();
                    cube.turnRightPrime();
                    cube.turnDownPrime();
                    cube.turnFront();
                    cube.turnDownPrime();
               //     Green = true;
                }

            }

            if (cube.getTopFace()[2][1] == 1) {
                // done
                if (cube.getFrontFace()[0][1] == 5) {
                    cube.turnUpPrime();
                    cube.turnRight();
                    cube.turnRight();
               //     Blue = true;
                } else if (cube.getFrontFace()[0][1] == 3) {
                    cube.turnFront();
                    cube.turnFront();
               //     Orange = true;
                } else if (cube.getFrontFace()[0][1] == 4) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnBack();
                    cube.turnBack();
              //      Red = true;
                } else if (cube.getFrontFace()[0][1] == 6) {
                    cube.turnUp();
                    cube.turnLeft();
                    cube.turnLeft();
              //      Green = true;
                }

            }

            if (cube.getTopFace()[1][0] == 1) {
                // done
                if (cube.getLeftFace()[0][1] == 5) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnRight();
                    cube.turnRight();
              //      Blue = true;
                } else if (cube.getLeftFace()[0][1] == 3) {
                    cube.turnUpPrime();
                    cube.turnFront();
                    cube.turnFront();
              //      Orange = true;
                } else if (cube.getLeftFace()[0][1] == 4) {
                    cube.turnUp();
                    cube.turnBack();
                    cube.turnBack();
             //       Red = true;
                } else if (cube.getLeftFace()[0][1] == 6) {
                    cube.turnLeft();
                    cube.turnLeft();
             //       Green = true;
                }

            }

            if (cube.getTopFace()[0][1] == 1) {

                if (cube.getBackFace()[2][1] == 5) {
                    cube.turnUp();
                    cube.turnRight();
                    cube.turnRight();
             //       Blue = true;
                } else if (cube.getBackFace()[2][1] == 3) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnFront();
                    cube.turnFront();
              //      Orange = true;
                } else if (cube.getBackFace()[2][1] == 4) {

                    cube.turnBack();
                    cube.turnBack();
              //      Red = true;
                } else if (cube.getBackFace()[2][1] == 6) {
                    cube.turnUpPrime();
                    cube.turnLeft();
                    cube.turnLeft();
              //      Green = true;
                }

            }

            if (cube.getTopFace()[1][2] == 1) {

                if (cube.getRightFace()[0][1] == 5) {
                    cube.turnRight();
                    cube.turnRight();
               //     Blue = true;
                } else if (cube.getRightFace()[0][1] == 3) {
                    cube.turnUp();
                    cube.turnFront();
                    cube.turnFront();
                //    Orange = true;
                } else if (cube.getRightFace()[0][1] == 4) {
                    cube.turnUpPrime();
                    cube.turnBack();
                    cube.turnBack();
              //      Red = true;
                } else if (cube.getRightFace()[0][1] == 6) {
                    cube.turnUp();
                    cube.turnUp();
                    cube.turnLeft();
                    cube.turnLeft();
             //       Green = true;
                }

            }

            if (cube.getFrontFace()[2][1] == 1) {
                cube.turnFront();
            }
            if (cube.getLeftFace()[2][1] == 1) {
                cube.turnLeft();
            }
            if (cube.getRightFace()[2][1] == 1) {
                cube.turnRight();
            }
            if (cube.getBackFace()[0][1] == 1) {
                cube.turnBack();
            }

            if (cube.getBottomFace()[0][1] == 1 && cube.getFrontFace()[2][1] != 3) {
                cube.turnFront();
            }
            if (cube.getBottomFace()[1][0] == 1 && cube.getLeftFace()[2][1] != 6) {
                cube.turnLeft();
            }
            if (cube.getBottomFace()[1][2] == 1 && cube.getRightFace()[2][1] != 5) {
                cube.turnRight();
            }
            if (cube.getBottomFace()[2][1] == 1 && cube.getBackFace()[0][1] != 4) {
                
                System.out.println("HERE");
                cube.turnBack();
            }
            count++;

        }

    }

    static public void solveFirstLayer(Cube cube) {
        int count = 0;
        
        while(count < 25){
        if(cube.getBottomFace()[0][0] == 1 && cube.getFrontFace()[2][0] == 3
        && cube.getBottomFace()[0][2] == 1 && cube.getFrontFace()[2][2] == 3    
        && cube.getBottomFace()[2][0] == 1 && cube.getBackFace()[0][0] == 4
        && cube.getBottomFace()[2][2] == 1 && cube.getBackFace()[0][2] == 4){
            break;
        }
        
      
        
        
        
        //WHITE IS ON THE FRONT FACE
        if(cube.getRightFace()[0][0] == 1){
            cube.turnUp();
        }
        else if(cube.getBackFace()[2][2] == 1){
            cube.turnUp();
            cube.turnUp();
        }
        else if(cube.getLeftFace()[0][0]  == 1){
            cube.turnUpPrime();
        }
     
        if(cube.getFrontFace()[0][0] == 1){
            if(cube.getTopFace()[2][0]  == 6){
                cube.turnUp();
                cube.turnLeft();
                cube.turnUp();
                cube.turnLeftPrime();
                //redgreen
            } else if(cube.getTopFace()[2][0]  == 5){
                cube.turnUpPrime();
                cube.turnRight();
                cube.turnUp();
                cube.turnRightPrime();
                //blueorange
            } else if(cube.getTopFace()[2][0]  == 3){
                cube.turnFront();
                cube.turnUp();
                cube.turnFrontPrime();
                //greenorange
            } else if(cube.getTopFace()[2][0]  == 4){
            cube.turnUp();
            cube.turnUp();
            cube.turnBack();
            cube.turnUp();
            cube.turnBackPrime();
            //bluered
        }
    }

       
        
        //WHITE IS ON THE LEFT FACE
        if(cube.getRightFace()[0][2] == 1){
            cube.turnUp();
            cube.turnUp();
        }
        else if(cube.getBackFace()[2][0] == 1){
            cube.turnUpPrime();
        }
        else if(cube.getFrontFace()[0][2]  == 1){
            cube.turnUp();
        }

        System.out.println("hi"); 
        if(cube.getLeftFace()[0][2] == 1){
             System.out.println("hi");
            if(cube.getTopFace()[2][0]  == 6){
                cube.turnLeftPrime();
                cube.turnUpPrime();
                cube.turnLeft();
                //greenorange
                
            } else if(cube.getTopFace()[2][0]  == 5){
                cube.turnUp();
                cube.turnUp();
                cube.turnRightPrime();
                cube.turnUpPrime();
                cube.turnRight();
                //bluered
            } else if(cube.getTopFace()[2][0]  == 3){
                cube.turnUpPrime();
                cube.turnFrontPrime();
                cube.turnUpPrime();
                cube.turnFront();
                //blueorange
            } else if(cube.getTopFace()[2][0]  == 4){
            cube.turnUp();
            cube.turnBackPrime();
            cube.turnUpPrime();
            cube.turnBack();
            //greenred
        }
            
            System.out.println("hi"); 
        if(cube.getBottomFace()[0][0] == 1 && cube.getFrontFace()[2][0] != 3){
            cube.turnRight();
            cube.turnUp();
            cube.turnRightPrime();
        }
        if(cube.getBottomFace()[0][2] == 1 && cube.getFrontFace()[2][2] != 3){
            cube.turnLeftPrime();
            cube.turnUp();
            cube.turnLeft();
        }
        if(cube.getBottomFace()[2][0] == 1 && cube.getBackFace()[0][0] != 4){
            cube.turnBackPrime();
            cube.turnUpPrime();
            cube.turnBack();
        }
        if(cube.getBottomFace()[2][2] == 1 && cube.getBackFace()[0][2] != 4){
            cube.turnBack();
            cube.turnUp();
            cube.turnBackPrime();
        }
        
        }  

        
        //blueorange meet
        if(cube.getLeftFace()[2][0] == 1){
            cube.turnLeft();
            cube.turnUp();
            cube.turnLeftPrime();
        } else if(cube.getFrontFace()[2][2] == 1){
            cube.turnRight();
            cube.turnUpPrime();
            cube.turnRightPrime();
        //greenorange meet    
        } else if(cube.getFrontFace()[2][0] == 1){
            cube.turnLeftPrime();
            cube.turnUp();
            cube.turnLeft();
        } else if(cube.getLeftFace()[2][2] == 1){
            cube.turnLeftPrime();
            cube.turnUpPrime();
            cube.turnLeft();
        //blueredmeet
        } else if(cube.getBackFace()[0][2] == 1){
            cube.turnBack();
            cube.turnUp();
            cube.turnBackPrime();
        } else if(cube.getRightFace()[2][2] == 1){
            cube.turnBack();
            cube.turnUpPrime();
            cube.turnBackPrime();
        //greenred meet
        } else if(cube.getBackFace()[0][0] == 1){
            cube.turnBackPrime();
            cube.turnUpPrime();
            cube.turnBack();
        } else if(cube.getFrontFace()[2][2] == 1){
            cube.turnBackPrime();
            cube.turnUp();
            cube.turnBack();
        }
        
        
    //TOP IS WHITE    
        
    if(cube.getTopFace()[2][2] == 1){
        if(cube.getBottomFace()[0][2] != 1){
        cube.turnRight();
        cube.turnUp();
        cube.turnUp();
        cube.turnRightPrime();
        }
        else cube.turnUp();
    } else if(cube.getTopFace()[2][0] == 1){
        if(cube.getBottomFace()[0][0] != 1){
        cube.turnLeftPrime();
        cube.turnUp();
        cube.turnUp();
        cube.turnLeft();
        }
        else cube.turnUp();
    } else if(cube.getTopFace()[0][0] == 1){
        if(cube.getBottomFace()[2][0] != 1){
            cube.turnBackPrime();
            cube.turnUp();
            cube.turnUp();
            cube.turnBack();
            }
            else cube.turnUp();
    } else if(cube.getTopFace()[0][2] == 1){
        if(cube.getBottomFace()[2][0] != 1){
            cube.turnBack();
            cube.turnUpPrime();
            cube.turnBackPrime();
            }
            else cube.turnUp();
    }
        

        
        count++;
        
        }
        
  
        
        
        
    }    
    
       public static void RightSolve(Cube cube){
        cube.turnUp();
        cube.turnRight();
        cube.turnUpPrime();
        cube.turnRightPrime();
        cube.turnUpPrime();
        cube.turnFrontPrime();
        cube.turnUp();
        cube.turnFront();
    }
       
       public static void LeftSolve(Cube cube){
        cube.turnUpPrime();
        cube.turnLeftPrime();
        cube.turnUp();
        cube.turnLeft();
        cube.turnUp();
        cube.turnFront();
        cube.turnUpPrime();
        cube.turnFrontPrime();
    }
    
    static public void solveSecond(Cube cube) {
        if(cube.getFrontFace()[0][1] == 3 && cube.getTopFace()[2][1] == 6 ){
            RightSolve(cube);
        }
         if(cube.getFrontFace()[0][1] == 3 && cube.getTopFace()[2][1] == 6 ){
            RightSolve(cube);
        }
         if(cube.getFrontFace()[0][1] == 3 && cube.getTopFace()[2][1] == 6 ){
            RightSolve(cube);
        }
         if(cube.getFrontFace()[0][1] == 3 && cube.getTopFace()[2][1] == 6 ){
            RightSolve(cube);
        }
 
    
    
    
    
    }
    
    
    
    
    
    
    
        
}