public class Cube {

    // we will assume that yellow is on top, orange facing you, white on bottom

    // 1 is white
    // 2 is yellow
    // 3 is orange
    // 4 is red
    // 5 is blue
    // 6 is green

    private int[][] FrontFace;
    private int[][] BackFace;
    private int[][] TopFace;
    private int[][] LeftFace;
    private int[][] RightFace;
    private int[][] BottomFace;

    public Cube(int[][] FrontFace, int[][] BackFace, int[][] TopFace, int[][] LeftFace, int[][] RightFace,
            int[][] BottomFace) {

        this.FrontFace = FrontFace;
        this.BackFace = BackFace;
        this.TopFace = TopFace;
        this.LeftFace = LeftFace;
        this.RightFace = RightFace;
        this.BottomFace = BottomFace;
    }
    
    
    public void rightRotation(int[][] FrontFace, int[][] BackFace, int[][] TopFace, int[][] LeftFace, int[][] RightFace,
            int[][] BottomFace){
        
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);
        
        
        
        TopFace[0][0] = TopFacetemp[2][0];
        TopFace[0][1] = TopFacetemp[1][0];
        TopFace[0][2] = TopFacetemp[0][0];
        TopFace[1][0] = TopFacetemp[2][1];
        TopFace[1][2] = TopFacetemp[0][1];
        TopFace[2][0] = TopFacetemp[2][2];
        TopFace[2][1] = TopFacetemp[1][2];
        TopFace[2][2] = TopFacetemp[0][2];
        
        BottomFace[0][0] = BottomFacetemp[0][2];
        BottomFace[0][1] = BottomFacetemp[1][2];
        BottomFace[0][2] = BottomFacetemp[2][2];
        BottomFace[1][0] = BottomFacetemp[0][1];
        BottomFace[1][2] = BottomFacetemp[2][1];
        BottomFace[2][0] = BottomFacetemp[0][0];
        BottomFace[2][1] = BottomFacetemp[1][0];
        BottomFace[2][2] = BottomFacetemp[2][0];
        
        FrontFace = maketempFace(LeftFacetemp);
        RightFace = maketempFace(FrontFacetemp);
        LeftFace[0][0] = BackFacetemp[2][2];
        LeftFace[0][1] = BackFacetemp[2][1];
        LeftFace[0][2] = BackFacetemp[2][0];
        LeftFace[1][0] = BackFacetemp[1][2];
        LeftFace[1][1] = BackFacetemp[1][1];
        LeftFace[1][2] = BackFacetemp[1][0];
        LeftFace[2][0] = BackFacetemp[0][2];
        LeftFace[2][1] = BackFacetemp[0][1];
        LeftFace[2][2] = BackFacetemp[0][0];
        
        BackFace[0][0] = RightFacetemp[2][2];
        BackFace[0][1] = RightFacetemp[2][1];
        BackFace[0][2] = RightFacetemp[2][0];
        BackFace[1][0] = RightFacetemp[1][2];
        BackFace[1][1] = RightFacetemp[1][1];
        BackFace[1][2] = RightFacetemp[1][0];
        BackFace[2][0] = RightFacetemp[0][2];
        BackFace[2][1] = RightFacetemp[0][1];
        BackFace[2][2] = RightFacetemp[0][0];
 
        
        
        
    }
    
    
    public void leftRotation(int[][] FrontFace, int[][] BackFace, int[][] TopFace, int[][] LeftFace, int[][] RightFace,
            int[][] BottomFace){
        
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);
        
        
        
        TopFace[0][0] = TopFacetemp[0][2];
        TopFace[0][1] = TopFacetemp[1][2];
        TopFace[0][2] = TopFacetemp[2][2];
        TopFace[1][0] = TopFacetemp[0][1];
        TopFace[1][2] = TopFacetemp[2][1];
        TopFace[2][0] = TopFacetemp[0][0];
        TopFace[2][1] = TopFacetemp[1][0];
        TopFace[2][2] = TopFacetemp[2][0];
        
        BottomFace[0][0] = BottomFacetemp[2][0];
        BottomFace[0][1] = BottomFacetemp[1][0];
        BottomFace[0][2] = BottomFacetemp[0][0];
        BottomFace[1][0] = BottomFacetemp[2][1];
        BottomFace[1][2] = BottomFacetemp[0][1];
        BottomFace[2][0] = BottomFacetemp[2][2];
        BottomFace[2][1] = BottomFacetemp[1][2];
        BottomFace[2][2] = BottomFacetemp[0][2];
        
        FrontFace = maketempFace(RightFacetemp);
        LeftFace = maketempFace(FrontFacetemp);
        RightFace[0][0] = BackFacetemp[2][2];
        RightFace[0][1] = BackFacetemp[2][1];
        RightFace[0][2] = BackFacetemp[2][0];
        RightFace[1][0] = BackFacetemp[1][2];
        RightFace[1][1] = BackFacetemp[1][1];
        RightFace[1][2] = BackFacetemp[1][0];
        RightFace[2][0] = BackFacetemp[0][2];
        RightFace[2][1] = BackFacetemp[0][1];
        RightFace[2][2] = BackFacetemp[0][0];
        
        BackFace[0][0] = LeftFacetemp[2][2];
        BackFace[0][1] = LeftFacetemp[2][1];
        BackFace[0][2] = LeftFacetemp[2][0];
        BackFace[1][0] = LeftFacetemp[1][2];
        BackFace[1][1] = LeftFacetemp[1][1];
        BackFace[1][2] = LeftFacetemp[1][0];
        BackFace[2][0] = LeftFacetemp[0][2];
        BackFace[2][1] = LeftFacetemp[0][1];
        BackFace[2][2] = LeftFacetemp[0][0];
 
        
        
        
    }
    
    
    public void printCube(int[][] FrontFace, int[][] BackFace, int[][] TopFace, int[][] LeftFace, int[][] RightFace,
            int[][] BottomFace){

        int i;
        int j;
        
        System.out.println("Top");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(TopFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        System.out.println("Front");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(FrontFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");



        System.out.println("Bottom");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BottomFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Right");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(RightFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Left");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(LeftFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Back");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BackFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        
    
    }
    
    
    
    public int[][] maketempFace(int[][] Face){
    
        int[][] Temp = new int[3][3];
    for(int i = 0; i < Face.length; i ++) {
        for(int j = 0; j< Face[i].length; j++) {
            Temp[i][j] = Face[i][j];
        }
    }
    return Temp;

    }
    
    
    public int[][] getFrontFace() {
        return this.FrontFace;
    }

    public int[][] getBackFace() {
        return this.BackFace;
    }

    public int[][] getTopFace() {
        return this.TopFace;
    }

    public int[][] getLeftFace() {
        return this.LeftFace;
    }

    public int[][] getRightFace() {
        return this.RightFace;
    }

    public int[][] getBottomFace() {
        return this.BottomFace;
    }

    public void turnRight() {
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);;
        int[][] BackFacetemp = maketempFace(BackFace);;
        int[][] TopFacetemp = maketempFace(TopFace);;
        int[][] RightFacetemp = maketempFace(RightFace);;
        int[][] BottomFacetemp = maketempFace(BottomFace);;

        for (int i = 0; i <= 2; i++) {
            FrontFace[i][2] = BottomFacetemp[i][2];
            TopFace[i][2] = FrontFacetemp[i][2];
            BackFace[i][2] = TopFacetemp[i][2];
            BottomFace[i][2] = BackFacetemp[i][2];
        }

        RightFace[0][0] = RightFacetemp[2][0];
        RightFace[0][1] = RightFacetemp[1][0];
        RightFace[0][2] = RightFacetemp[0][0];
        RightFace[1][0] = RightFacetemp[2][1];
        RightFace[1][2] = RightFacetemp[0][1];
        RightFace[2][0] = RightFacetemp[2][2];
        RightFace[2][1] = RightFacetemp[1][2];
        RightFace[2][2] = RightFacetemp[0][2];
        System.out.println("R ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnRightPrime() {
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);

        for (int i = 0; i <= 2; i++) {
            FrontFace[i][2] = TopFacetemp[i][2];
            TopFace[i][2] = BackFacetemp[i][2];
            BackFace[i][2] = BottomFacetemp[i][2];
            BottomFace[i][2] = FrontFacetemp[i][2];
        }

        RightFace[0][0] = RightFacetemp[0][2];
        RightFace[0][1] = RightFacetemp[1][2];
        RightFace[0][2] = RightFacetemp[2][2];
        RightFace[1][0] = RightFacetemp[0][1];
        RightFace[1][2] = RightFacetemp[2][1];
        RightFace[2][0] = RightFacetemp[0][0];
        RightFace[2][1] = RightFacetemp[1][0];
        RightFace[2][2] = RightFacetemp[2][0];

        System.out.print("R' ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnLeft() {
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);

        for (int i = 0; i <= 2; i++) {
            FrontFace[i][0] = TopFacetemp[i][0];
            TopFace[i][0] = BackFacetemp[i][0];
            BackFace[i][0] = BottomFacetemp[i][0];
            BottomFace[i][0] = FrontFacetemp[i][0];
        }

        LeftFace[0][0] = LeftFacetemp[2][0];
        LeftFace[0][1] = LeftFacetemp[1][0];
        LeftFace[0][2] = LeftFacetemp[0][0];
        LeftFace[1][0] = LeftFacetemp[2][1];
        LeftFace[1][2] = LeftFacetemp[0][1];
        LeftFace[2][0] = LeftFacetemp[2][2];
        LeftFace[2][1] = LeftFacetemp[1][2];
        LeftFace[2][2] = LeftFacetemp[0][2];
        System.out.println("L ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnLeftPrime() {
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);

        for (int i = 0; i <= 2; i++) {
            FrontFace[i][0] = BottomFacetemp[i][0];
            TopFace[i][0] = FrontFacetemp[i][0];
            BackFace[i][0] = TopFacetemp[i][0];
            BottomFace[i][0] = BackFacetemp[i][0];
        }

        LeftFace[0][0] = LeftFacetemp[0][2];
        LeftFace[0][1] = LeftFacetemp[1][2];
        LeftFace[0][2] = LeftFacetemp[2][2];
        LeftFace[1][0] = LeftFacetemp[0][1];
        LeftFace[1][2] = LeftFacetemp[2][1];
        LeftFace[2][0] = LeftFacetemp[0][0];
        LeftFace[2][1] = LeftFacetemp[1][0];
        LeftFace[2][2] = LeftFacetemp[2][0];

        System.out.println("L' ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

    }

    public void turnBack() {
        // done

        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);

    

        for (int i = 0; i <= 2; i++) {
            RightFace[i][2] = BottomFacetemp[2][2-i];
            TopFace[0][i] = RightFacetemp[i][2];
            LeftFace[2-i][0] = TopFacetemp[0][i];
            BottomFace[2][i] = LeftFacetemp[i][0];
        }

        BackFace[0][0] = BackFacetemp[2][0];
        BackFace[0][1] = BackFacetemp[1][0];
        BackFace[0][2] = BackFacetemp[0][0];
        BackFace[1][0] = BackFacetemp[2][1];
        BackFace[1][2] = BackFacetemp[0][1];
        BackFace[2][0] = BackFacetemp[2][2];
        BackFace[2][1] = BackFacetemp[1][2];
        BackFace[2][2] = BackFacetemp[0][2];
        
        System.out.println("B ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnBackPrime() {
        // done

        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);

        for (int i = 0; i <= 2; i++) {
            RightFace[i][2] = TopFacetemp[0][i];
            TopFace[0][2-i] = LeftFacetemp[i][0];
            LeftFace[i][0] = BottomFacetemp[2][i];
            BottomFace[2][2-i] = RightFacetemp[i][2];
        }

        BackFace[0][0] = BackFacetemp[0][2];
        BackFace[0][1] = BackFacetemp[1][2];
        BackFace[0][2] = BackFacetemp[2][2];
        BackFace[1][0] = BackFacetemp[0][1];
        BackFace[1][2] = BackFacetemp[2][1];
        BackFace[2][0] = BackFacetemp[0][0];
        BackFace[2][1] = BackFacetemp[1][0];
        BackFace[2][2] = BackFacetemp[2][0];

        System.out.println("B' ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnDown() { // for what
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);

        for (int i = 0; i <= 2; i++) {
            FrontFace[2][i] = LeftFacetemp[2][i];
            LeftFace[2][i] = BackFacetemp[0][2 - i];
            BackFace[0][2 - i] = RightFacetemp[2][i];
            RightFace[2][i] = FrontFacetemp[2][i];
        }

        BottomFace[0][0] = BottomFacetemp[2][0];
        BottomFace[0][1] = BottomFacetemp[1][0];
        BottomFace[0][2] = BottomFacetemp[0][0];
        BottomFace[1][0] = BottomFacetemp[2][1];
        BottomFace[1][2] = BottomFacetemp[0][1];
        BottomFace[2][0] = BottomFacetemp[2][2];
        BottomFace[2][1] = BottomFacetemp[1][2];
        BottomFace[2][2] = BottomFacetemp[0][2];
        System.out.println("D ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnDownPrime() {
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);

        for (int i = 0; i <= 2; i++) {
            FrontFace[2][i] = RightFacetemp[2][i];
            LeftFace[2][i] = FrontFacetemp[2][i];
            BackFace[0][2 - i] = LeftFacetemp[2][i];
            RightFace[2][i] = BackFacetemp[0][2 - i];
        }

        BottomFace[0][0] = BottomFacetemp[0][2];
        BottomFace[0][1] = BottomFacetemp[1][2];
        BottomFace[0][2] = BottomFacetemp[2][2];
        BottomFace[1][0] = BottomFacetemp[0][1];
        BottomFace[1][2] = BottomFacetemp[2][1];
        BottomFace[2][0] = BottomFacetemp[0][0];
        BottomFace[2][1] = BottomFacetemp[1][0];
        BottomFace[2][2] = BottomFacetemp[2][0];

        System.out.println("D'");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnFront() {
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);

        for (int i = 0; i <= 2; i++) {
            TopFace[2][i] = LeftFacetemp[2-i][2];
            LeftFace[i][2] = BottomFacetemp[0][i];
            BottomFace[0][i] = RightFacetemp[2-i][0];
            RightFace[i][0] = TopFacetemp[2][i];
        }

        FrontFace[0][0] = FrontFacetemp[2][0];
        FrontFace[0][1] = FrontFacetemp[1][0];
        FrontFace[0][2] = FrontFacetemp[0][0];
        FrontFace[1][0] = FrontFacetemp[2][1];
        FrontFace[1][2] = FrontFacetemp[0][1];
        FrontFace[2][0] = FrontFacetemp[2][2];
        FrontFace[2][1] = FrontFacetemp[1][2];
        FrontFace[2][2] = FrontFacetemp[0][2];
        System.out.println("F ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnFrontPrime() {
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] BottomFacetemp = maketempFace(BottomFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);

        for (int i = 0; i <= 2; i++) {
            TopFace[2][i] = RightFacetemp[i][0];
            LeftFace[i][2] = TopFacetemp[2][2-i];
            BottomFace[0][i] = LeftFacetemp[i][2];
            RightFace[i][0] = BottomFacetemp[0][2-i];
        }

        FrontFace[0][0] = FrontFacetemp[0][2];
        FrontFace[0][1] = FrontFacetemp[1][2];
        FrontFace[0][2] = FrontFacetemp[2][2];
        FrontFace[1][0] = FrontFacetemp[0][1];
        FrontFace[1][2] = FrontFacetemp[2][1];
        FrontFace[2][0] = FrontFacetemp[0][0];
        FrontFace[2][1] = FrontFacetemp[1][0];
        FrontFace[2][2] = FrontFacetemp[2][0];

        System.out.println("F' ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnUp() {
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);

        for (int i = 0; i <= 2; i++) {
            FrontFace[0][i] = RightFacetemp[0][i];
            RightFace[0][i] = BackFacetemp[2][2 - i];
            BackFace[2][2 - i] = LeftFacetemp[0][i];
            LeftFace[0][i] = FrontFacetemp[0][i];
        }

        TopFace[0][0] = TopFacetemp[2][0];
        TopFace[0][1] = TopFacetemp[1][0];
        TopFace[0][2] = TopFacetemp[0][0];
        TopFace[1][0] = TopFacetemp[2][1];
        TopFace[1][2] = TopFacetemp[0][1];
        TopFace[2][0] = TopFacetemp[2][2];
        TopFace[2][1] = TopFacetemp[1][2];
        TopFace[2][2] = TopFacetemp[0][2];
        System.out.println("U ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
    }

    public void turnUpPrime() {
        // done
        int[][] FrontFacetemp = maketempFace(FrontFace);
        int[][] BackFacetemp = maketempFace(BackFace);
        int[][] TopFacetemp = maketempFace(TopFace);
        int[][] RightFacetemp = maketempFace(RightFace);
        int[][] LeftFacetemp = maketempFace(LeftFace);;

        for (int i = 0; i <= 2; i++) {
            FrontFace[0][i] = LeftFacetemp[0][i];
            RightFace[0][i] = FrontFacetemp[0][i];
            BackFace[2][2 - i] = RightFacetemp[0][i];
            LeftFace[0][i] = BackFacetemp[2][2 - i];
        }

        TopFace[0][0] = TopFacetemp[0][2];
        TopFace[0][1] = TopFacetemp[1][2];
        TopFace[0][2] = TopFacetemp[2][2];
        TopFace[1][0] = TopFacetemp[0][1];
        TopFace[1][2] = TopFacetemp[2][1];
        TopFace[2][0] = TopFacetemp[0][0];
        TopFace[2][1] = TopFacetemp[1][0];
        TopFace[2][2] = TopFacetemp[2][0];
        System.out.println("U' ");
        printCube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

    }

}