import static org.junit.Assert.*;

import org.junit.Test;

public class SolveTest {

  //  @Test
    public void turnRight() {
        int i;
        int j;
        int[][] FrontFace = new int[3][3];
        int[][] BackFace = new int[3][3];
        int[][] TopFace = new int[3][3];
        int[][] BottomFace = new int[3][3];
        int[][] LeftFace = new int[3][3];
        int[][] RightFace = new int[3][3];

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                FrontFace[i][j] = 3;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BackFace[i][j] = 4;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                TopFace[i][j] = 2;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BottomFace[i][j] = 1;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                RightFace[i][j] = 5;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                LeftFace[i][j] = 6;
            }
        }

        Cube cube = new Cube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

        cube.turnRightPrime();

        
        System.out.println("Top");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(TopFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        System.out.println("Front");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(FrontFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");



        System.out.println("Bottom");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BottomFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Right");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(RightFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Left");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(LeftFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Back");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BackFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        
        
        
        
        

    }

 //   @Test
    public void turnLeft() {
        int i;
        int j;
        int[][] FrontFace = new int[3][3];
        int[][] BackFace = new int[3][3];
        int[][] TopFace = new int[3][3];
        int[][] BottomFace = new int[3][3];
        int[][] LeftFace = new int[3][3];
        int[][] RightFace = new int[3][3];

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                FrontFace[i][j] = 3;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BackFace[i][j] = 4;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                TopFace[i][j] = 2;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BottomFace[i][j] = 1;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                RightFace[i][j] = 5;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                LeftFace[i][j] = 6;
            }
        }

        Cube cube = new Cube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

        cube.turnLeftPrime();
        
    
        
        
        
        /*
        * for(i=0; i<3; i++){ for(j=0; j<3; j++){
        * System.out.print(FrontFace[i][j] + " "); } } System.out.println("");
        */
    }

//    @Test
    public void turnDown() {
        int i;
        int j;
        int[][] FrontFace = new int[3][3];
        int[][] BackFace = new int[3][3];
        int[][] TopFace = new int[3][3];
        int[][] BottomFace = new int[3][3];
        int[][] LeftFace = new int[3][3];
        int[][] RightFace = new int[3][3];

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                FrontFace[i][j] = 3;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BackFace[i][j] = 4;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                TopFace[i][j] = 2;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BottomFace[i][j] = 1;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                RightFace[i][j] = 5;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                LeftFace[i][j] = 6;
            }
        }

        Cube cube = new Cube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

        cube.turnDownPrime();


    
        
        
        
        /*
        * for(i=0; i<3; i++){ for(j=0; j<3; j++){
        * System.out.print(FrontFace[i][j] + " "); } } System.out.println("");
        */
    }

 //   @Test
    public void turnUp() {
        int i;
        int j;
        int[][] FrontFace = new int[3][3];
        int[][] BackFace = new int[3][3];
        int[][] TopFace = new int[3][3];
        int[][] BottomFace = new int[3][3];
        int[][] LeftFace = new int[3][3];
        int[][] RightFace = new int[3][3];

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                FrontFace[i][j] = 3;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BackFace[i][j] = 4;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                TopFace[i][j] = 2;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BottomFace[i][j] = 1;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                RightFace[i][j] = 5;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                LeftFace[i][j] = 6;
            }
        }

        Cube cube = new Cube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

        cube.turnUpPrime();
        
        
        
        
    
        
        
        
        
        
        
        
        
        /*
        * for(i=0; i<3; i++){ for(j=0; j<3; j++){
        * System.out.print(FrontFace[i][j] + " "); } } System.out.println("");
        */
    }

    //@Test
    public void turnBack() {
        int i;
        int j;
        int[][] FrontFace = new int[3][3];
        int[][] BackFace = new int[3][3];
        int[][] TopFace = new int[3][3];
        int[][] BottomFace = new int[3][3];
        int[][] LeftFace = new int[3][3];
        int[][] RightFace = new int[3][3];

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                FrontFace[i][j] = 3;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BackFace[i][j] = 4;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                TopFace[i][j] = 2;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BottomFace[i][j] = 1;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                RightFace[i][j] = 5;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                LeftFace[i][j] = 6;
            }
        }

        Cube cube = new Cube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

        cube.turnBackPrime();


        
        
        /*
        * for(i=0; i<3; i++){ for(j=0; j<3; j++){
        * System.out.print(TopFace[i][j] + " "); } } System.out.println("");
        */
    }

  //  @Test
    public void turnFront() {
        int i;
        int j;
        int[][] FrontFace = new int[3][3];
        int[][] BackFace = new int[3][3];
        int[][] TopFace = new int[3][3];
        int[][] BottomFace = new int[3][3];
        int[][] LeftFace = new int[3][3];
        int[][] RightFace = new int[3][3];

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                FrontFace[i][j] = 3;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BackFace[i][j] = 4;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                TopFace[i][j] = 2;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BottomFace[i][j] = 1;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                RightFace[i][j] = 5;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                LeftFace[i][j] = 6;
            }
        }

        Cube cube = new Cube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

        cube.turnFrontPrime();
        
    
        

        /*
        * for(i=0; i<3; i++){ for(j=0; j<3; j++){
        * System.out.print(TopFace[i][j] + " "); } } System.out.println("");
        */
    }
  
    @Test
    public void RightRotation() {
        int i;
        int j;
        int[][] FrontFace = new int[3][3];
        int[][] BackFace = new int[3][3];
        int[][] TopFace = new int[3][3];
        int[][] BottomFace = new int[3][3];
        int[][] LeftFace = new int[3][3];
        int[][] RightFace = new int[3][3];

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                FrontFace[i][j] = 3;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BackFace[i][j] = 4;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                TopFace[i][j] = 2;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BottomFace[i][j] = 1;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                RightFace[i][j] = 5;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                LeftFace[i][j] = 6;
            }
        }

        Cube cube = new Cube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

        cube.rightRotation(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
        
    
        

        
        System.out.println("Top");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(TopFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        System.out.println("Front");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(FrontFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");



        System.out.println("Bottom");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BottomFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Right");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(RightFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Left");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(LeftFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Back");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BackFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        
        
    }
    
    
    @Test
    public void LeftRotation() {
        int i;
        int j;
        int[][] FrontFace = new int[3][3];
        int[][] BackFace = new int[3][3];
        int[][] TopFace = new int[3][3];
        int[][] BottomFace = new int[3][3];
        int[][] LeftFace = new int[3][3];
        int[][] RightFace = new int[3][3];

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                FrontFace[i][j] = 3;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BackFace[i][j] = 4;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                TopFace[i][j] = 2;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                BottomFace[i][j] = 1;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                RightFace[i][j] = 5;
            }
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                LeftFace[i][j] = 6;
            }
        }

        Cube cube = new Cube(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);

        cube.leftRotation(FrontFace, BackFace, TopFace, LeftFace, RightFace, BottomFace);
        
    
        
        
        System.out.println("Top");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(TopFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        System.out.println("Front");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(FrontFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");



        System.out.println("Bottom");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BottomFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Right");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(RightFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Left");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(LeftFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Back");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BackFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        
        
    }
  //  @Test
    public void testCross() {
        int i;
        int j;
        int[][] FrontFace = new int[3][3];
        int[][] BackFace = new int[3][3];
        int[][] TopFace = new int[3][3];
        int[][] BottomFace = new int[3][3];
        int[][] LeftFace = new int[3][3];
        int[][] RightFace = new int[3][3];

        FrontFace[0][0] = 4;
        FrontFace[0][1] = 4;
        FrontFace[0][2] = 1;
        FrontFace[1][0] = 3;
        FrontFace[1][1] = 3;
        FrontFace[1][2] = 2;
        FrontFace[2][0] = 1;
        FrontFace[2][1] = 3;
        FrontFace[2][2] = 2;

        BackFace[0][0] = 3;
        BackFace[0][1] = 1;
        BackFace[0][2] = 5;
        BackFace[1][0] = 1;
        BackFace[1][1] = 4;
        BackFace[1][2] = 4;
        BackFace[2][0] = 3;
        BackFace[2][1] = 3;
        BackFace[2][2] = 2;

        // 1 is white
        // 2 is yellow
        // 3 is orange
        // 4 is red
        // 5 is blue
        // 6 is green

        RightFace[0][0] = 4;
        RightFace[0][1] = 2;
        RightFace[0][2] = 5;
        RightFace[1][0] = 5;
        RightFace[1][1] = 5;
        RightFace[1][2] = 5;
        RightFace[2][0] = 6;
        RightFace[2][1] = 1;
        RightFace[2][2] = 3;

        LeftFace[0][0] = 6;
        LeftFace[0][1] = 4;
        LeftFace[0][2] = 5;
        LeftFace[1][0] = 5;
        LeftFace[1][1] = 6;
        LeftFace[1][2] = 6;
        LeftFace[2][0] = 6;
        LeftFace[2][1] = 2;
        LeftFace[2][2] = 6;

        // 1 is white
        // 2 is yellow
        // 3 is orange
        // 4 is red
        // 5 is blue
        // 6 is green

        TopFace[0][0] = 1;
        TopFace[0][1] = 1;
        TopFace[0][2] = 3;
        TopFace[1][0] = 2;
        TopFace[1][1] = 2;
        TopFace[1][2] = 3;
        TopFace[2][0] = 2;
        TopFace[2][1] = 6;
        TopFace[2][2] = 5;

        BottomFace[0][0] = 4;
        BottomFace[0][1] = 5;
        BottomFace[0][2] = 4;
        BottomFace[1][0] = 6;
        BottomFace[1][1] = 1;
        BottomFace[1][2] = 4;
        BottomFace[2][0] = 2;
        BottomFace[2][1] = 6;
        BottomFace[2][2] = 1;
        
        Cube cube = new Cube(FrontFace, BackFace, TopFace, LeftFace, RightFace,
                BottomFace);

        Solve.solveCross(cube);
        System.out.println("Cross Solved");





        
        System.out.println("Top");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(TopFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        System.out.println("Front");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(FrontFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");



        System.out.println("Bottom");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BottomFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Right");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(RightFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Left");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(LeftFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");

        System.out.println("Back");
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                System.out.print(BackFace[i][j] + " "); 
            } 
            System.out.println("");
        }
        System.out.println("");
        













        Solve.solveFirstLayer(cube);
        System.out.println("First Layer Solved");
    }

}