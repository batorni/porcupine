There are many files included, and are ran differently:

For the data structure of the cube there are three different classes: Solve, SolveTest, and Cube.

The cube implements the movements of the cube, while the solve class can solve the cross of a cube.  
The SolveTest class is not fully implemented, however, you can still do some basic testing.
Getting rid of the commented code for each of the classes will print out the FrontFace of the cube after the 
respective method is called.  You can change the face outputted by changing Front to whichever face you want.
At the top of cube, the numbers that correspond to each color is listed. The last method in SolveTest tests
the solver for the cross.  It does this by manually putting in each color on each face of each cube,  and
calls the solve method. It then prints out in the console each move, step by step. 

All of the other files are related to the Android Project.  By opening Android Studio, you can open the 
project.  The main part of the program displays a fully solved rubiks cube, and at the top displays the timer
and the solve.  Clicking on each of these will take you to that action, which are not implemented yet.

The camera portion allows you to take a picture, and then it will display in a 3 by 3 grid the colors that 
it thinks that it is.  This however, will not work in android studio as there is no camera implementation.  
If the project is moved to a phone, however, it will work.  To correctly align, put the cube at the very top
part of the camera, and make the width of the cube the same as the camera is wide, and take the picture.

Lastly, there is the part that displays a cube that is taken apart and laid out.  Clicking on each of the 
faces will then bring you to a screen where it will show which face of the cube that you should take a picture of.