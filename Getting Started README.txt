Hello Team PORCUPINE!
It's your Scrum Master here.

So, The easiest way to use bitbucket would be to download and install their client, SourceTree.
It's pretty simple.
Once you download/install SourceTree, just go online on BitBucket and click on our Repo.
Then click the big button that says "Clone in SourceTree"
This should boot up SourceTree on your PC and prompt you for a save location and such.
Boom! Done.

Just commit anything you work on, whether it be legitimate contributions to the project,
or just random test files. It'll help us all stay up to date on what we're working on and
maintain some contact.

I should be commiting some sort of image recognition thingy by the end of the night (10/10) if I get the chance.

Happy PORCUPINEing!

Edit:

So, after messing around with SourceTree a bit, here's a few things to note:
 * When you "clone" or "checkout" the source on your pc, SourceTree calls that your "master"
	The "master" branch is your own branch (I'm assuming), so it won't affect the "trunk"
	So, when you want to just commit your branch, you click "commit", choose what files to commit,
	comment your commit, then commit it.
	Once you do that, you're allowed to "Push" your changes. This is essentially a commit to the trunk
	where the trunk is the source that everyone can see. You select what you'd like to "push" and you push it.
	
 * When you're choosing what files to commit to your branch, there is an area on SourceTree that says "Unstaged files" and "Staged files"
	The staged files are files that will be commited with your next commit, and the unstaged, as you guessed it, won't be included in your commit.
	It's generally a good idea to not commit binary files.  Source Control systems like Git and SVN sorta hate them and cause problems. 
	
 * Another note for general source control: Don't push/commit anything to the trunk(main source) unless it is 
	tested and working.  Make everyone else aware of this push, so we can pull in the changes in our own branch.
	I generally notify everyone prior to the push to ensure nothing terrible can go wrong.
	
 * Generally speaking, your branch can be your own little playground. Do whatever you want to do to it. Just prior to
	pushing, make sure it works. Commits to your personal branch don't affect others if you did it right.
	
Sorry for my ramblings, but there's my quick notage and such.
